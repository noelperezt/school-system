
var selecciono_horario = false;
var validar_paso_dos = true;
var validar_paso_tres = true;
var primer_nivel = 0;
var edad_min = 0;
var edad = 0;
var edad_max = 0;
var procesar_inscripcion = true;

var monto_base = 0;
var monto_iva = 0;
var monto_total_pagar = 0;
var total_credito = 0;
var total_contado = 0;
$(function(){
    $('#id_rif_cliente, #id_cedula_alumno').mask('a-999999999');
    $('#id_fecha_nacimiento, #single_cal2').mask('99/99/9999');
    $('#id_telefono, #id_movil, #id_telefono_opcional, #movil, #telefono_opcional, #telefono').mask('999-9999999');    
    $('#id_antiguedad').keypress(function(e){
        if(e.keyCode == 13){
            $('.buttonNext').trigger( "click" );
        }
    })
    $('#id_fecha_nacimiento').keypress(function(e){
        if(e.keyCode == 13){
            edad = parseInt(calcularEdad( $(this).val() ));
            verificarEdadPermitidaCurso();
            $('#id_edad').val(edad);
            if(edad < 18){
                $('#id_cedula_alumno').val( $('#id_rif_cliente').val() );
            }    
        }
        
    });
    $('#direccion_habitacion').keypress(function(e){
        if(e.keyCode == 13 && $(this).val().trim() != ''){
            $('.buttonNext').trigger( "click" );
        }
    })
    $('#id_codigo_curso').change(function(){
        selecciono_horario = false;
        if ($(this).val() == ''){
            return;
        }
        NProgress.start();
        $.ajax({
            url: '/ventas/contrato/buscar-datos/',
            type: 'post',
            dataType: 'json',
            data: {accion : 'getNiveles', curso : $(this).val()},
            success: function (data) {
                NProgress.done();
                if(data.mensaje == ''){
                    $('#tb_niveles tbody').html(data.datos);
                    $('#monto_inscripcion').val(data.inscripcion);
                    $('#_inscripcion').text(numberFormat(data.inscripcion));

                    //
                }else{
                    mostrarMensaje(data.mensaje);
                }
            }
        });
    });
    $('body').on('click','.niveles', function(e){
        $('.niveles').each(function (index) {
            if(this.checked) {
                var tmp_primer_nivel = this.value;
                if(primer_nivel == 0 || (tmp_primer_nivel != primer_nivel)){
                    primer_nivel = tmp_primer_nivel;
                    buscar_oferta_horarios(primer_nivel);
                }
                return false;                    
            }
        });
    });
    $('body').on('click','.horario', function(){
        $(".horario").prop("checked", "");
        if(!$(this).prop("checked")){
            selecciono_horario = true;
            $(this).prop("checked", "checked");
            if ($(this).attr('edad_min') != ''){
                edad_min = $(this).attr('edad_min')
            }
            if ($(this).attr('edad_max') != ''){
                edad_max = $(this).attr('edad_max')
            }
            $('#oferta_curso').val($(this).attr('oferta'))
            $('.buttonNext').trigger( "click" );
            $('#id_rif_cliente').focus();
        }
    });
    

    $('#id_rif_cliente').keydown(function(e){
        if(e.keyCode == 13 && $(this).val().trim() != '')
            buscarDatosCliente($(this).val());
        if (e.keyCode == teclaCtrl)
        ctrlPressed = true;
        if (ctrlPressed && (e.keyCode == teclaF1)){
            $('#busqueda_sensitiva').val('');
            catalogoCliente(1, '');
        }
    });
    $('#id_razon_social').keydown(function(e){
        if(e.keyCode == 13){
            if (validar_paso_dos == false){
                $('.buttonNext').trigger( "click" );    
            }
            
        }
    });
    $('#id_cedula_alumno').keydown(function(e){
        if(e.keyCode == 13 && $(this).val().trim() != '')
            buscarDatosAlumno($(this).val());
        if (e.keyCode == teclaCtrl)
        ctrlPressed = true;
        if (ctrlPressed && (e.keyCode == teclaF1)){
            $('#busqueda_sensitiva').val('');
            catalogoAlumnos(1, '');
        }
    });
    $('#id_apellido_alumno').keydown(function(e){
        if(!validar_paso_tres && e.keyCode == 13){
            $('.buttonNext').trigger( "click" );   
        }
    });

    $('input[name="forma_pago"]').click(function(){
        seleccionTotalPagarContrato();
    });
    
});
function focus_input_paso(obj, context){
    switch (obj[0].hash) {
        case '#step-2':
            $('#id_rif_cliente').focus();
            break;
        case '#step-3':
            $('#id_cedula_alumno').focus();        
            break;
        case '#step-4':
            break;

    }
}
function siguiente_paso(obj, context){
    
    switch (obj[0].hash) {
        case '#step-1':
            buscarMontoNiveles();
            return validarPasoUno();
            break;
        case '#step-2':
            return validarPasoDos();
            break;
        case '#step-3':
            return validarPasoTres();
            break;
        case '#step-4':
            return true;
            break;
        default:

    }
    return true;
}
function validarPasoDos(){
    var retorno;
    if(!validar_paso_dos){
        return true;
    }
    if(!validarForm($("#step-2 input"))){
        return false;

    }
    NProgress.start();
    $.ajax({
        url: '/ventas/contrato/registrar-cliente/',
        type: 'post',
        dataType: 'json',
        async:false,
        data:  $('#step-2 :input').serialize(),
        success: function (data) {
            NProgress.done();
            if(data.success == false){
                $.each(data.errors.fields, function(i, item) {
                    mostrarMensaje(i+ " "+item);
                });
                retorno = false;
                validar_paso_dos = true;
            }else{
                $('.stepContainer').css({'height':'400px'});
                $("html, body").animate({ scrollTop: '150px'}, 1000);                
                $('#id_cedula_alumno').focus();
                retorno = true;
                validar_paso_dos = false;
            }
        },
        error: function (error) {
            mostrarMensaje(error);
            retorno = false;
            validar_paso_dos = true;
        }
    });
    return retorno;    
    
}
function validarPasoTres(){
    var retorno;
    if(!validar_paso_tres){
        return true;
    }
    if(!validarForm($("#step-3 input"))){
        return false;
    }
    NProgress.start();    
    $.ajax({
        url: '/curso/alumno/registrar/',
        type: 'post',
        dataType: 'json',
        async:false,
        data:  $('#step-3 :input').serialize(),
        success: function (data) {
            NProgress.done();
            if(data.success == false){
                $.each(data.errors.fields, function(i, item) {
                    mostrarMensaje(i+ " "+item);
                });
                validar_paso_tres =  true;
                retorno =  false;
            }else{
                $('.stepContainer').css({'height':'400px'});
                $("html, body").animate({ scrollTop: '150px'}, 1000);
                $('#id_cedula_alumno').focus();
                validar_paso_tres =  false;
                retorno =  true;
                $('.buttonNext').trigger( "click" );

            }
        },
        error: function (error) {
            mostrarMensaje(error);
            retorno =  false;
        }
    });
    return retorno;
}

function validarPasoUno(){
    if(selecciono_horario){
        $('#id_rif_cliente').focus();
        return true;
    }else{
        return false;
    }
}
function buscar_oferta_horarios(nivel){
    NProgress.start();
    $('#tb_horarios tbody').html('');
    $.ajax({
        url: '/ventas/contrato/buscar-datos/',
        type: 'post',
        dataType: 'json',
        data:  {accion : 'getHorario', nivel : nivel},
        success: function (data) {
            NProgress.done();
            if(data.mensaje == ''){
                $('#tb_horarios tbody').html(data.datos);
            }else{
                mostrarMensaje(data.mensaje);
            }
        }
    });
}
function seleccion_alumno(cedula, nombre, apellido){
    $('#id_cedula_alumno').val(cedula);
    $('#id_nombre_alumno').val(nombre);
    $('#id_apellido_alumno').val(apellido);
    $('#from_catalogo').modal('hide');
    $('#id_apellido_alumno').focus();
    validar_paso_tres = false;
}
function seleccion_cliente(rif, nombre){
    $('#id_rif_cliente').val(rif);
    $('#id_razon_social').val(nombre);
    $('#from_catalogo').modal('hide');
    $('#id_razon_social').focus();
    validar_paso_dos = false;
}
function buscarDatosCliente(){
    NProgress.start();
    $.ajax({
        url: '/ventas/contrato/buscar-datos/',
        type: 'post',
        dataType: 'json',
        data:  {accion : 'getDatoCliente', rif_cliente: $('#id_rif_cliente').val()},
        success: function (data) {
            NProgress.done();
            if(data.datos == '' && data.mensaje == ''){
                validar_paso_dos = true;
                $('.stepContainer').css({'height':'576px'});
                $("html, body").animate({ scrollTop: '250px'}, 1000);
                $('#id_razon_social').focus();
                $('#completar_titular').fadeIn();
            }else{
                validar_paso_dos = false;
                $('.stepContainer').css({'height':'310px;'});
                $("html, body").animate({ scrollTop: '250px'}, 1000);
                $('#id_razon_social').val(data.razon_social);
            }
        }
    });
}

function buscarDatosAlumno(){
    NProgress.start();
    $.ajax({
        url: '/ventas/contrato/buscar-datos/',
        type: 'post',
        dataType: 'json',
        data:  {accion : 'getDatosAlumno', cedula : $('#id_cedula_alumno').val()},
        success: function (data) {
            
            if(data.datos == '' && data.mensaje == ''){
                validar_paso_tres = true;
                $('#completar_alumno').fadeIn();
                $('#id_nombre_alumno').focus();
                $('.stepContainer').css({'height':'400px'});
                $("html, body").animate({ scrollTop: '150px'}, 1000);                
                if( $('#id_cedula_alumno').val() == $('#id_rif_cliente').val() ){
                    copiarDatosBasicos();
                }
            }else{
                validar_paso_tres = false;                
                $('#id_nombre_alumno').val(data.nombre);
                $('#id_apellido_alumno').val(data.apellido);
                $('#id_fecha_nacimiento').val(data.fecha_nacimiento);
                var e = $.Event("keypress");
                e.keyCode = 13;
                $('#id_fecha_nacimiento').trigger(e);
                $('.buttonNext').trigger( "click" );
                NProgress.done();
            }
        }
    });
}

function buscarMontoNiveles(){
    json_niveles =  bsucarNivelesSeleccinados();
    var json = JSON.stringify(json_niveles);
    NProgress.start();
    $.ajax({
        url: '/ventas/contrato/buscar-datos/',
        type: 'post',
        dataType: 'json',
        data:  {accion : 'getMontoNiveles', niveles : json},
        success: function (data) {
            NProgress.done();
            $('#niveles_montos > tbody').html(data.datos);
            total_credito = data.totales.credito
            total_contado = data.totales.contado
            $('#total_credito').text( numberFormat(data.totales.credito) ) ;
            $('#total_contado').text(numberFormat(data.totales.contado));
            $('#tasa_iva').val(numberFormat(data.iva));
            $('#tasa_iva_porcentaje').append(' '+data.iva+' %')
            $('#numero_cuotas').val(data.cuotas)
            seleccionTotalPagarContrato();
        }
    });
}
function bsucarNivelesSeleccinados(){
    jsonObj = [];
    $('.niveles').each(function (index) {
        if(this.checked) {
            jsonObj.push(this.value);
        }
    });
    return jsonObj;
}

function copiarDatosBasicos(){
    $('#id_nombre_alumno').val()
    $('#id_nombre_alumno').val()
    $('#telefono').val( $('#id_telefono').val() )
    $('#movil').val( $('#id_movil').val() )
    $('#telefono_opcional').val( $('#id_telefono_opcional').val() )
    $('#correo').val( $('#id_correo').val() )
    $('#id_fecha_nacimiento').val( $('#single_cal2').val() )
    $('#direccion_habitacion').val( $('#id_direccion_habitacion').val() )
    
    var e = $.Event("keypress");
    e.keyCode = 13;
    $('#id_fecha_nacimiento').trigger(e);
    
    NProgress.done();
}

function verificarEdadPermitidaCurso(){
    if(edad >= edad_min && edad <= edad_max){
        procesar_inscripcion = true;
    }else{
        procesar_inscripcion = false;
    }
}

function seleccionTotalPagarContrato(){


    $("input[name=forma_pago]").each(function (index) {  
        if($(this).is(':checked')){
                tipo_contrato = $(this).val();
            if(tipo_contrato == 'Credito'){
                monto_base = total_credito;


            }else{

                monto_base = total_contado;
            }
            monto_iva = ( parseFloat(monto_base) * parseFloat($('#tasa_iva').val()) ) / 100
            monto_contrato = parseFloat(monto_base) + parseFloat(monto_iva)
            $('#monto_total_pagar').text(numberFormat(monto_contrato));
            $('#monto_contrato').val(monto_contrato);
            
            $('#monto_base').text( numberFormat(monto_base) )
            $('#iva').val(monto_iva)

            $('#monto_iva').text( numberFormat(monto_iva) )
        }
       
    });

    
}