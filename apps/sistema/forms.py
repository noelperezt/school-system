from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth.forms import UserCreationForm
from .models import Feriados, Usuario



class FeriadoForm(forms.ModelForm):
    INPUT_FORMAT = (
        ('%d/%m/%Y','%m/%d/%Y')
    )
    fecha = forms.DateField(input_formats = INPUT_FORMAT)
    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)
        for campo in self.fields:
            self.fields[campo].widget.attrs.update({'required' : 'required'})
            self.fields[campo].widget.attrs.update({'class' : 'form-control'})

        self.fields['fecha'].widget.attrs.update({'id' : 'single_cal2'})
    class Meta:
        model = Feriados
        fields = ['fecha', 'descripcion']
    


class UsuarioForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for campo in self.fields:
            if campo == 'user_permissions':
                self.fields[campo].widget.attrs.update({'style': 'height:140px; width:100%'})
            else:
                if campo != 'is_active':
                    self.fields[campo].widget.attrs.update({'required': 'required', 'class' : 'form-control'})
        self.fields['is_active'].widget.attrs.update({'class' : 'check'})     
         
    class Meta:
        model = Usuario
        fields = ['first_name','last_name','email', 'username','departamento','is_superuser',
            'is_active','user_permissions']

class UsuarioFormUpdate(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(UsuarioFormUpdate, self).__init__(*args, **kwargs)
        f = self.fields.get('user_permissions')
        if f is not None:
            f.queryset = f.queryset.select_related('content_type')

        for campo in self.fields:
            if campo == 'user_permissions':
                self.fields[campo].widget.attrs.update({'style': 'height:140px; width:100%'})
            else:
                if campo != 'is_active':
                    self.fields[campo].widget.attrs.update({'required': 'required', 'class' : 'form-control'})
        self.fields['is_active'].widget.attrs.update({'class' : 'check'}) 
        
    class Meta:
        model = Usuario
        fields = ['first_name', 'last_name', 'email', 'username', 'departamento', 'is_superuser', 'is_active',
                  'user_permissions']





