from django.db import models
from apps.administracion.models import Departamento
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _


# Create your models here.
class Usuario(AbstractUser):
    departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE, null=True, blank=True,
                                     verbose_name=_('Departamento'))
    cedula = models.CharField(max_length=16, null=False, blank=False, verbose_name=_('Nro. Identificación'))

    class Meta:
        db_table = 'tb_sis_usuario'
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'

    def get_cedula(self):
        return self.cedula


class Feriados(models.Model):
    fecha = models.DateField(verbose_name='Fecha festiva no laborable')
    descripcion = models.CharField(max_length=60, null=False, blank=False)

    class Meta:
        db_table = 'tb_sis_feriado'
        verbose_name = 'Feriado'
        verbose_name_plural = 'Feriados'



