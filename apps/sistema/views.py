from django.db import IntegrityError, transaction
from django.shortcuts import render, redirect, render_to_response
from django.template import RequestContext
from django.views.generic import ListView, UpdateView, CreateView, DeleteView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core import serializers
from django.core.urlresolvers import reverse
from django.contrib.auth.forms import PasswordChangeForm
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import Permission
from .forms import UsuarioForm,UsuarioFormUpdate, FeriadoForm
from .models import Feriados,Usuario
from apps.sistema.util.Funciones import cursorDictFetchall
import json


def index(request):

    if request.user.is_authenticated():
        return render(request, 'dashboard.html', {'titulo':_('Sistema financiero y de control de estudio')})
    else:
        if request.method == 'POST':
            if inicio_sesion(request):
                if request.GET:
                    return HttpResponseRedirect(request.GET.get('next'))
                else:
                    return render(request, 'dashboard.html', {'titulo':_('Sistema financiero y de control de estudio')})
            else:
                return render(request, 'login.html', {'titulo':_('Sistema financiero y de control de estudio')})    
        
    return render(request, 'login.html', {'titulo':_('Sistema financiero y de control de estudio')})

def inicio_sesion(request):
    
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return True        
        else:
            messages.add_message(request, messages.INFO, _('Cuenta de usuario inactiva'))
            return False
    else:
        messages.add_message(request, messages.INFO, _('Nombre de usuario o contraseña no valido'))

        

    #return HttpResponse(json.dumps(mensaje), content_type="application/json")
def planes(request):
    return render(request, 'front/planes.html', {'titulo': _('Planes y tarifas - Xliip Soft.')})

def cerrar_sesion(request):
    logout(request)
    return redirect('/')

'''
def paginacion_permisos(request):
    datos = ''

    if request.GET.get('model'):
        datos = Permission.objects.filter(content_type_id=request.GET.get('model')).order_by('id')
    else:
        datos = Permission.objects.all().order_by('id')


    return datos'''
'''
def crearPerfil(request):
    try:
        
        nuevo_grupo = Group(
            name=request.POST['name'],
        )
        nuevo_grupo.save()
        obj_grupo_nuevo = Group.objects.get(pk=nuevo_grupo.id)
        
        lista_permisos = request.POST.getlist('checkbox[]')
        for permiso in lista_permisos:
            obj_grupo_nuevo.permissions.add(Permission.objects.get(pk=permiso))

    except IntegrityError as e:
        pass '''


class Usuario_Registro(CreateView):
    model = Usuario
    form_class = UsuarioForm
    template_name = 'usuario.html'
    success_url = reverse_lazy('sistemas:usuario_listado')

    def get_context_data(self, **kwargs):
        context = super(Usuario_Registro, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = 'Registrar usuario en el sistema'

        return context

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            try:
                with transaction.atomic():
                    usuario = form.save()
                    for item in request.POST.getlist('permisos_seleccionados'):
                        usuario.user_permissions.add(Permission.objects.get(pk=item))
            except IntegrityError as e:
                print(e)
        else:
            messages.add_message(request, messages.WARNING, form.errors)
        return HttpResponseRedirect(self.get_success_url())


class Usuario_Actualizar(UpdateView):
    model = Usuario
    form_class = UsuarioFormUpdate
    template_name = 'usuario.html'
    success_url = reverse_lazy('sistemas:usuario_listado')

    def get_context_data(self, **kwargs):
        context = super(Usuario_Actualizar, self).get_context_data(**kwargs)
        if 'accion' not in context:
            context['accion'] = 'actualizar'
        if 'titulo' not in context:
            context['titulo'] = 'Actualiar datos del usuario'

        return context

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, instance=Usuario.objects.get(pk=self.kwargs.get('pk', 0)))
        if form.is_valid():
            try:
                with transaction.atomic():
                    usuario = form.save()
                    Permission.objects.filter(user=usuario).delete()
                    for item in request.POST.getlist('permisos_seleccionados'):
                        usuario.user_permissions.add(Permission.objects.get(pk=item))

            except IntegrityError as e:
                print(e)
        else:
            messages.add_message(request, messages.WARNING, form.errors)

        return HttpResponseRedirect('/usuario/listado')


class Usuario_Listado(ListView):
    model = Usuario
    template_name = 'listado_usuario.html'
    form_class = UsuarioForm
    paginate_by = 10


class Usuario_Eliminar(DeleteView):
    model = Usuario
    template_name = 'usuario_eliminar.html'
    success_url = reverse_lazy('sistemas:usuario_listado')

    def get_context_data(self, **kwargs):
        context = super(Usuario_Eliminar, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = 'Eliminar datos del usuario'

        return context


class Feriado_Registro(CreateView):
    model = Feriados
    form_class = FeriadoForm
    template_name = 'feriados.html'
    success_url = reverse_lazy('sistemas:feriado_listado')

    def get_context_data(self, **kwargs):
        context = super(Feriado_Registro, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Registro de Días feriados')
        return context


class Listado_Feriados(ListView):
    model = Feriados
    template_name = 'listado_feriados.html'
    paginate_by = 10
    def get_context_data(self, **kwargs):
        context = super(Listado_Feriados, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Listado de Días feriados')

        return context

class Feriado_Actualizar(UpdateView):
    model  = Feriados
    form_class = FeriadoForm
    template_name = 'feriados.html'
    success_url = reverse_lazy('sistemas:feriado_listado')

    def get_context_data(self, **kwargs):
        context = super(Feriado_Actualizar, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Actualizar Días feriados')

        return context

class Feriado_Eliminar(DeleteView):
    model = Feriados
    template_name = 'eliminar_feriado.html'
    success_url = reverse_lazy('sistemas:feriado_listado')

'''
def gestionarAjax(request):
    #if request.POST['accoin'] == 'retornar_opciones':
    opciones = Permission.objects.filter(content_type=request.POST['model'])
    respuesta = serializers.serialize('json', opciones)
    return HttpResponse(respuesta, content_type="application/json")
    #return HttpResponse(json.dumps({'hola':'asdas d'}), content_type="application/json")


def Rol_registrar(request):
    if request.is_ajax():
        return gestionarAjax(request)

    accion = ''
    context = {
        'titulo': _('Asignar roles a usuarios'),
        'form2' : ContentType.objects.filter(id__gt=5),
        'accion': accion
    }
    return render(request, 'rol_usuario.html', context) '''

def Usuario_Cambio_Clave(request):
    
    if request.method == "POST":
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')
    else:
        form = PasswordChangeForm(request.user)
        form.fields['old_password'].widget.attrs = {'class': 'form-control','required':'required',
                                                    'label': _('Contraseña actual')}
        form.fields['new_password1'].widget.attrs = {'class': 'form-control','required':'required',
                                                     'label':_('Nueva contraseña')}
        form.fields['new_password2'].widget.attrs = {'class': 'form-control','required':'required',
                                                     'label': _('Confirme su nueva contraseña')}

    context = {
        'titulo': _('Cambiar contraseña de acceso'),
        'form': form
    }

    return render(request, 'cambiar_clave.html', context)
