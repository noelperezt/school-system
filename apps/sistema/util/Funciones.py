from django.http import JsonResponse
from django.http import HttpResponse
import json
def cursorDictFetchall(cursor):
    '''
    Returns all rows from a cursor as a dict
    '''
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
        ]
