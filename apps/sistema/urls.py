from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required
from .views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^$', index),
    #url(r'^inicio-sesion/$', inicio_sesion, name='inicio_sesion'),
    url(r'^cerrar-sesion/$', cerrar_sesion),
    url(r'^usuario/registro/$', login_required(Usuario_Registro.as_view()), name='usuario_crear'),
    url(r'^usuario/editar/(?P<pk>\d+)/$', login_required(Usuario_Actualizar.as_view()), name='usuario_actualizar'),
    url(r'^usuario/listado', login_required(Usuario_Listado.as_view()), name='usuario_listado'),
    url(r'^usuario/cambiar-clave/', login_required(Usuario_Cambio_Clave), name='usuario_cambio_clave'),
    

    #url(r'^usuario/rol/registrar/$', login_required(Rol_registrar), name='rol_registrar'),
    
    url(r'^configuracion/dias-feriados/listado', login_required(Listado_Feriados.as_view()), name='feriado_listado'),
    url(r'^configuracion/dias-feriados/$', login_required(Feriado_Registro.as_view()), name='feriado_crear'),
    url(r'^configuracion/dias-feriados/editar/(?P<pk>\d+)/$', login_required(Feriado_Actualizar.as_view()), name='feriado_actualizar'),
    url(r'^configuracion/dias-feriados/eliminar/(?P<pk>\d+)/$', login_required(Feriado_Eliminar.as_view()), name='feriado_actualizar'),

    
    url(r'^planes/$', planes),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
