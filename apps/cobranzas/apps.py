from django.apps import AppConfig


class CobranzasConfig(AppConfig):
    name = 'cobranzas'
