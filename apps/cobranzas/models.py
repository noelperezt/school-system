from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from apps.administracion.models import Cliente


class CuentaCobrar(models.Model):
    DEUDA = 'D'
    PAGADO = 'P'
    ANULADO = 'A'
    STATUS = (
        (DEUDA, 'Deuda'),
        (PAGADO, 'Pagado'),
        (ANULADO, 'Anulado'),
    )
    descripcion = models.CharField(max_length=100, null=False, blank=False, verbose_name=_('Descripción'))
    fecha_registro = models.DateTimeField(default=timezone.now, verbose_name=_('Fecha registro de la cuenta'))
    monto_cuenta_base = models.DecimalField(max_digits=3, decimal_places=1, null=False, blank=False,
                                            verbose_name='Monto de la cuenta sin iva')
    monto_cuenta = models.DecimalField(max_digits=3, decimal_places=1, null=False, blank=False,
                                       verbose_name='Monto de la cuenta')
    saldo = models.DecimalField(max_digits=3, decimal_places=1, null=False, blank=False,
                                verbose_name='Saldo de la cuenta')
    cliente = models.ForeignKey(Cliente, db_index=True, on_delete=models.PROTECT)

    class Meta:
        db_table = 'cpc_cuenta_cobrar'
        verbose_name = 'Cuenta por cobrar'
        verbose_name_plural = 'Cuentas por cobrar'

class AbonoCuentaCobrar(models.Model):
    fecha_registro = models.DateTimeField(default=timezone.now, verbose_name=_('Fecha registro del pago'))
    monto_pagado = models.DecimalField(max_digits=3, decimal_places=1, null=False, blank=False,
                                            verbose_name='Monto del pago')
    cuenta_cobrar = models.ForeignKey(CuentaCobrar, on_delete=models.PROTECT)

    class Meta:
        db_table = 'cpc_abono_cuenta_cobrar'
        verbose_name = 'Abono Cuenta por cobrar'
        verbose_name_plural = 'Abonos Cuenta por cobrar'