from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render
from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.forms.models import modelform_factory
from django.core.urlresolvers import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.db import IntegrityError, transaction, connection
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, UpdateView, CreateView, DeleteView
from .forms import *
from .forms import CongiguracionForm
from .models import *
from apps.curso.transaccional.models import Cronograma
import json

# Create your views here.


class Salon_Registro(CreateView):
    model = Salon
    form_class = SalonForm
    template_name = 'curso/salones.html'
    success_url = reverse_lazy('curso:salon_listado')
    def get_context_data(self, **kwargs):
        context = super(Salon_Registro, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Registar salones de clases')

        return context

class Salon_Actualizar(UpdateView):
    model = Salon
    form_class = SalonForm
    template_name = 'curso/salones.html'
    success_url = reverse_lazy('curso:salon_listado')
    def get_context_data(self, **kwargs):
        context = super(Salon_Actualizar, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Actualizar salones de clases')
        if 'accion' not in context:
            context['accion'] = 'actualizar'

        return context


class Salon_Listado(ListView):
    model = Salon
    form_class = SalonForm
    template_name = 'curso/salones_listado.html'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(Salon_Listado, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Listado de salones de clases')

        return context

class Salon_Eliminar(DeleteView):
    model = Salon
    success_url = reverse_lazy('curso:salon_listado')
    template_name = 'curso/salon_eliminar.html'


class Curso_Crear(CreateView):
    model = Curso
    form_class = CursoForm
    template_name = 'curso/curso.html'
    success_url = reverse_lazy('curso:curso_listado')

    def get_context_data(self, **kwargs):
        context = super(Curso_Crear, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Listado de cursos')

        return context

class Curso_Actualizar(UpdateView):
    model = Curso
    form_class = CursoForm
    template_name = 'curso/curso.html'
    success_url = reverse_lazy('curso:curso_listado')

    def get_context_data(self, **kwargs):
        context = super(Curso_Actualizar, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Actualizar datos del curso')
        if 'accion' not in context:
            context['accion'] = 'actualizar'
        return context

class Curso_Eliminar(DeleteView):
    model = Curso
    success_url = reverse_lazy('curso:curso_listado')
    template_name = 'curso/curso_eliminar.html'

class Curso_Listado(ListView):
    model = Curso
    form_class = CursoForm
    template_name = 'curso/curso_listado.html'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(Curso_Listado, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Listado de cursos')

        return context


def Nivel_Contexto(pk_curso):
    data_curso = Curso.objects.get(pk=pk_curso)
    listado_nivel = Nivel.objects.filter(
                        codigo_curso=Curso.objects.get(pk=pk_curso))

    context = {
        'form': modelform_factory(Curso, fields=('codigo_curso', 'descripcion','id')),
        'form_nivel': NivelForm,
        'data_curso': data_curso,
        'listado_nivel': listado_nivel ,
        'titulo': _('Asignar niveles a curso'),
    }
    return context
def Curso_Crear_Niveles(request, pk_curso):
    if request.method == 'POST':
        frm_nivel = NivelForm(request.POST)
        if frm_nivel.is_valid():
            frm_nivel.save()
            reverse_lazy('curso:niveles_crear')
        else:
            messages.add_message(request, messages.WARNING, frm_nivel.errors)

    context = Nivel_Contexto(pk_curso)
    context['accion'] = 'agregar_nivel'
    return render(request, 'curso/niveles.html', Nivel_Contexto(pk_curso))

def Curso_Actualizar_Niveles(request, pk_curso, pk_nivel):
    if request.method == 'POST':
        frm_nivel = NivelForm(request.POST, instance=Nivel.objects.get(pk=pk_nivel))
        if frm_nivel.is_valid():
            frm_nivel.save()
            return HttpResponseRedirect('/curso/niveles/%s/' % ( pk_curso ))

    data_curso = Nivel.objects.filter(pk=pk_nivel)
    context = Nivel_Contexto(pk_curso)
    context['data_nivel'] = data_curso[0]
    context['accion'] = 'editar_nivel'
    return render(request, 'curso/niveles.html', context)

class Curso_Eliminar_Niveles(DeleteView):
    model = Nivel
    template_name = 'curso/niveles_eliminar.html'
    success_url = reverse_lazy('curso:curso_listado')

    def get_context_data(self, **kwargs):
        context = super(Curso_Eliminar_Niveles, self).get_context_data(**kwargs)
        if 'curso_pk' not in context:
            context['curso_pk'] = self.kwargs['r_pk']

        return context

    def delete(self, request, *args, **kwargs):
        Nivel.objects.filter(pk=self.kwargs['pk']).delete()
        return HttpResponseRedirect('/curso/niveles/%s/' % (self.kwargs['r_pk']))


class Profesor_Listado(ListView):
    model = Profesor
    template_name = 'curso/profesor_listado.html'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(Profesor_Listado, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Listado de profesor registrados en el sistema')

        return context

class Profesor_Crear(CreateView):
    model = Profesor
    form_class = ProfesorForm
    template_name = 'curso/profesor.html'
    success_url = reverse_lazy('curso:profesor_listado')

    def get_context_data(self, **kwargs):
        context = super(Profesor_Crear, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Registrar profesor')

        return  context

    def form_invalid(self, form):        
        if not form.is_valid():
            messages.add_message(self.request, messages.WARNING, form.errors)
        return self.render_to_response(self.get_context_data(form=form))



class Profesor_Actualizar(UpdateView):
    model = Profesor
    form_class = ProfesorForm
    template_name = 'curso/profesor.html'
    success_url = reverse_lazy('curso:profesor_listado')

    def get_context_data(self, **kwargs):
        context = super(Profesor_Actualizar, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Actualizar datos de profesor')
        if 'accion' not in context:
            context['accion'] = 'actualizar'

        return  context

    def form_invalid(self, form):

        if not form.is_valid():

            messages.add_message(self.request, messages.WARNING, form.errors)
        return self.render_to_response(self.get_context_data(form=form))

class Profesor_Eliminar(DeleteView):
    model = Profesor
    template_name = 'curso/profesor_eliminar.html'
    success_url = reverse_lazy('curso:profesor_listado')
    def get_context_data(self, **kwargs):
        context = super(Profesor_Eliminar, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Eliminar profesor')

        return  context


class Horario_Listado(ListView):
    model = Horario
    template_name = 'curso/horario_listado.html'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(Horario_Listado, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Listado de Horarios')

        return context


def Validar_HorarioForm(request):
    if not len(request.POST['codigo_horario']) > 0:
        messages.add_message(request, messages.WARNING, _("El código de horario es un campo requerido"))
        return False
    if not len(request.POST['descripcion']) > 0:
        messages.add_message(request, messages.WARNING, _("El descripción de horario es un campo requerido"))
        return False
    if len(request.POST.getlist('dia[]')) == 0:
        messages.add_message(request, messages.WARNING, _("Por favor agregue un día de clases al horario"))
        return False

    return True
def Horario_Crear(request):
    if request.method == 'POST':
        if Validar_HorarioForm(request):
            lista_dia = request.POST.getlist('dia[]')
            lista_hora_inicio = request.POST.getlist('inicio[]')
            lista_hora_culminacion = request.POST.getlist('fin[]')
            try:
                with transaction.atomic():
                    obj_horario = Horario(
                        codigo_horario=request.POST['codigo_horario'],
                        descripcion=request.POST['descripcion']
                    )
                    obj_horario.save()
                    for i in range(0, len(lista_dia)):
                        Horario_detalle(
                            dia_semana=lista_dia[i],
                            hora_inicio=lista_hora_inicio[i],
                            hora_culminacion=lista_hora_culminacion[i],
                            codigo_horario=obj_horario
                        ).save()
            except IntegrityError as e:
                messages.add_message(request, messages.WARNING, _("Ocurrió un error inesperado y no se logro registrar el horario"))

            return HttpResponseRedirect('/curso/horario/listado/')


    context = {
        'form': HorarioForm,
        'titulo': _('Asignar niveles a curso'),
        'total_registro' : 0
    }
    return render(request, 'curso/horario.html', context)


class Horario_Actualizar(UpdateView):
    model = Horario
    form_class = HorarioForm
    template_name = 'curso/horario.html'
    success_url = reverse_lazy('curso:horario_listado')

    def get_context_data(self, **kwargs):
        context = super(Horario_Actualizar, self).get_context_data(**kwargs)
        pk = self.kwargs['pk']
        detalle_horario = Horario_detalle.objects.filter(codigo_horario=Horario.objects.get(pk=pk))
        if 'titulo' not in context:
            context['titulo'] = _('Editar datos del Horarios')
        if 'accion' not in context:
            context['accion'] = 'actualizar'
        if 'form' not in context:
            context['form'] = self.form_class
        if 'object_list' not in context:
            context['object_list'] = detalle_horario
        if 'total_registro' not in context:
            context['total_registro'] = detalle_horario.count()

        context['id '] = pk
        return  context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()

        if not len(request.POST['descripcion']) > 0:
            messages.add_message(request, messages.WARNING, _("El descripción de horario es un campo requerido"))
            return HttpResponseRedirect('/curso/horario/editar/%s/' % (self.kwargs['pk']))

        obj_horario = Horario.objects.filter(pk=self.kwargs['pk'])

        #Despues que esta en estado Ocupado no se puede actulizar el estado por esta vita
        if(obj_horario[0].estado == 'O'):
            obj_horario.update(
                descripcion=request.POST['descripcion']
            )
        else:
            #tampoco se permite colocar a un horario como ocupado desde esta vista
            if request.POST['estado'] == 'O':
                obj_horario.update(
                    descripcion=request.POST['descripcion'],
                )
            else:
                obj_horario.update(
                    descripcion=request.POST['descripcion'],
                    estado=request.POST['estado'],
                )
        #guardar nuevos dias de clases
        if len(request.POST.getlist('dia[]')) > 0:
            lista_dia = request.POST.getlist('dia[]')
            lista_hora_inicio = request.POST.getlist('inicio[]')
            lista_hora_culminacion = request.POST.getlist('fin[]')
            for i in range(0, len(lista_dia)):
                Horario_detalle(
                    dia_semana=lista_dia[i],
                    hora_inicio=lista_hora_inicio[i],
                    hora_culminacion=lista_hora_culminacion[i],
                    codigo_horario=Horario.objects.get(pk=self.kwargs['pk'])
                ).save()
        return HttpResponseRedirect('/curso/horario/editar/%s/' % (self.kwargs['pk']))

class Horario_Eliminar(DeleteView):
    model = Horario
    template_name = 'curso/horario_eliminar.html'
    success_url = reverse_lazy('curso:horario_listado')

    def get_context_data(self, **kwargs):
        context = super(Horario_Eliminar, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Eliminar horario')

        return  context

class Horario_Eliminar_Detalle(DeleteView):
    model = Horario_detalle
    template_name = 'curso/horario_detalle_eliminar.html'
    success_url = reverse_lazy('curso:horario_listado')
    def get_context_data(self, **kwargs):
        context = super(Horario_Eliminar_Detalle, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Eliminar horario')
        if 'horario_pk' not in context:
            context['horario_pk'] = self.kwargs['r_pk']

        return  context


def Validar_PensumForm(request):
    if not len(request.POST['codigo_pensum']) > 0:
        messages.add_message(request, messages.WARNING,_("El código de pensum es un campo requerido"))
        return False
    if not len(request.POST['hora_semanal']) > 0:
        messages.add_message(request, messages.WARNING, _("La horas semanales del pensum es un campo requerido"))
        return False
    if not len(request.POST['descripcion']) > 0:
        messages.add_message(request, messages.WARNING, _("La descripcion del pensum es un campo requerido"))
        return False
    if len(request.POST.getlist('unidad[]')) == 0:
        messages.add_message(request, messages.WARNING, _("Por favor agregue el detalle del pensum"))
        return False

    return True

def Pensum_Crear(request):
    if request.method == 'POST':
        if Validar_PensumForm(request):
            lista_unidad = request.POST.getlist('unidad[]')
            lista_actividad = request.POST.getlist('actividad_detalle[]')
            lista_clase = request.POST.getlist('nro_clase[]')
            try:
                with transaction.atomic():
                    obj_pensum = Pensum(
                        codigo_pensum=request.POST['codigo_pensum'],
                        descripcion=request.POST['descripcion'],
                        hora_semanal=request.POST['hora_semanal'],
                        estado=request.POST['estado'],
                    )
                    obj_pensum.save()
                    for i in range(0, len(lista_unidad)):
                        Pensum_detalle(
                            numero_unidad=lista_unidad[i],
                            numero_clase=lista_clase[i],
                            actividad=lista_actividad[i],
                            codigo_pensum=obj_pensum
                        ).save()
            except IntegrityError as e:
                messages.error(request, _("Ocurrió un error inesperado y no se logro registrar el pensum"))

            return HttpResponseRedirect('/curso/pensum/listado/')


    context = {
        'form': PensumForm,
        'titulo': _('Crear Pensum académico'),
        'total_registro' : 0
    }
    return render(request, 'curso/pensum.html', context)


class Pensum_Listado(ListView):
    model = Pensum
    template_name = 'curso/pensum_listado.html'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(Pensum_Listado, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Listado de Pensum')

        return context


class Pensum_Actualizar(UpdateView):
    model = Pensum
    form_class = PensumForm
    template_name = 'curso/pensum.html'
    success_url = reverse_lazy('curso:pensum_listado')

    def get_context_data(self, **kwargs):
        context = super(Pensum_Actualizar, self).get_context_data(**kwargs)
        pk = self.kwargs['pk']
        detalle_pensum = Pensum_detalle.objects.filter(codigo_pensum=Pensum.objects.get(pk=pk))
        if 'titulo' not in context:
            context['titulo'] = _('Editar datos del Pensum')
        if 'accion' not in context:
            context['accion'] = 'actualizar'
        if 'form' not in context:
            context['form'] = self.form_class
        if 'object_list' not in context:
            context['object_list'] = detalle_pensum
        if 'total_registro' not in context:
            context['total_registro'] = detalle_pensum.count()
        if 'pk' not in context:
            context['pk'] = pk

        context['id '] = pk
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()

        if not Validar_PensumForm(request):
            return HttpResponseRedirect('/curso/pensum/editar/%s/' % (self.kwargs['pk']))

        obj_pensum= Pensum.objects.filter(pk=self.kwargs['pk'])
        obj_pensum.update(
            descripcion=request.POST['descripcion'],
            hora_semanal=request.POST['hora_semanal'],
            estado=request.POST['estado'],
        )

        #guardar nuevas cargas academicas
        
        if len(request.POST.getlist('unidad[]')) > 0:
            lista_unidad = request.POST.getlist('unidad[]')
            lista_actividad = request.POST.getlist('actividad_detalle[]')
            lista_clase = request.POST.getlist('nro_clase[]')
            for i in range(0, len(lista_unidad)):
                Pensum_detalle(
                    numero_unidad=lista_unidad[i],
                    numero_clase=lista_clase[i],
                    actividad=lista_actividad[i],
                    codigo_pensum=Pensum.objects.get(pk=self.kwargs['pk'])
                ).save()
        
        return HttpResponseRedirect('/curso/pensum/editar/%s/' % (self.kwargs['pk']))
class Pensum_Eliminar(DeleteView):
    model = Pensum
    template_name = 'curso/pensum_eliminar.html'
    success_url = reverse_lazy('curso:pensum_listado')

    def get_context_data(self, **kwargs):
        context = super(Pensum_Eliminar, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Eliminar pensum')
        if 'pensum_pk' not in context:
            context['pensum_pk'] = self.kwargs['r_pk']
        return context
def Pensum_Eliminar_Todo_Pensum(request, pk_pensum):
    datos = Cronograma.objects.filter(pensum_detalle=Pensum_detalle.objects.filter(codigo_pensum=Pensum.objects.get(id=pk_pensum)))

    if not datos:
        Pensum_detalle.objects.filter(codigo_pensum=Pensum.objects.get(id=pk_pensum)).delete()

    return HttpResponseRedirect('/curso/pensum/editar/%s/' % (pk_pensum))

class Pensum_Eliminar_Detalle(DeleteView):
    model = Pensum_detalle
    template_name = 'curso/pensum_detalle_eliminar.html'
    success_url = reverse_lazy('curso:pensum_listado')

    def get_context_data(self, **kwargs):
        context = super(Pensum_Eliminar_Detalle, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Eliminar pensum')
        if 'pensum_pk' not in context:
            context['pensum_pk'] = self.kwargs['r_pk']

        return context


class ResponseAjax(object):
    def form_invalid(self, form):
        to_json = {}
        to_json.update(success=False)
        if form.errors:
            errors = {}
            fields = {}
            for field_name, text in form.errors.items():
                fields[field_name] = text

            errors.update(fields=fields)
            to_json.update(errors=errors)
        return HttpResponse(json.dumps(to_json), content_type="application/json")

    def form_valid(self, form):
        self.object = form.save()
        context = {'success': True}
        return HttpResponse(json.dumps(context), content_type="application/json")


class Alumno_Registrar(ResponseAjax, CreateView):
    model = Alumno
    form_class = AlumnoForm
    def get_context_data(self, context):
        context['success'] = True
        return context


def DatosCurso(request):
    mensaje = ''
    try:
        datos_cursos = Curso.objects.get(codigo_curso=request.POST['codigo_curso'])
        return HttpResponse(json.dumps({
            'mensaje':'',
            'nombre_curso' : datos_cursos.descripcion
        }),
            content_type="application/json")
    except ObjectDoesNotExist:
        mensaje =  _('Código de curso ingresado no se encuentra registrado')
        return HttpResponse(json.dumps(
            {'mensaje': str(mensaje)}),
            content_type="application/json")

def Catalogo_curso(request):
    xHTML = ''
    mensaje = ''
    if 'frase' in request.POST and len(request.POST['frase']) > 0:
        Crusos = Curso.objects.filter(estado='A', descripcion=request.POST['frase'])
    else:
        Crusos = Curso.objects.filter(estado='A')

    paginator = Paginator(Crusos, 10)

    try:
        page = request.POST['pagina']
        datos = paginator.page(page)
    except PageNotAnInteger:
        datos = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        datos = paginator.page(paginator.num_pages)
    paginas = datos.previous_page_number


    for curso in datos:
        xHTML += '<tr>'
        xHTML += '<td><a style = "cursor:pointer" onclick="seleccion_curso(\'' + curso.codigo_curso + '\',\'' + str(
            curso.descripcion) + '\')">' + curso.codigo_curso + '</a></td>'
        xHTML += '<td>' + curso.descripcion + '</td>'
        xHTML += '</tr>'

    return HttpResponse(json.dumps({'datos': xHTML, 'mensaje': str(mensaje), 'pagina': str(paginas)}),
                        content_type="application/json")

def Configuracion_Academico(request):
    if request.method == 'POST':
        Configuracion.objects.all().delete()
        form = CongiguracionForm(request.POST)
        if form.is_valid():
            form.save()
            cursos_creados = Salon.objects.all().count()
            print(cursos_creados)
            if cursos_creados == 0:
                messages.add_message(request, messages.WARNING, 'A Continuación registre los salones de clases')
                return HttpResponseRedirect('/curso/salon/registrar/')
            else:
                messages.add_message(request, messages.INFO, 'Configuracion aplicada con exito')
                return HttpResponseRedirect('/curso/configuracion/')
        else:
            messages.add_message(request, messages.WARNING, form.errors)
    context = {
        'form': CongiguracionForm,
        'titulo': _('Configuración módulo académico'),
    }
    return render(request, 'curso/configuracion_general.html', context)