from django.db import models
from django.utils.translation import ugettext_lazy as _
from apps.administracion.models import ConceptoFactura
# Create your models here.

class Configuracion(models.Model):
    oferta_salon_culminar_clases = models.IntegerField(default=0,null=False, blank=False,
                                                       verbose_name=_('Permitir oferta salon antes de culminar clases'))
    extiende_inscripcion = models.IntegerField(default=0, null=False, blank=False,
                                               verbose_name=_('Días que se extenderá las inscripciones para completar '
                                                              'capacidad máxima del salon'))
    configuracio_aplicada = models.BooleanField(default=False)

    class Meta:
        db_table = 'tb_cde_configuracion'
        verbose_name = 'Configuracion'
        verbose_name_plural = 'Configuraciones'

class Pensum(models.Model):
    ACTIVO = 'A'
    INACTIVO = 'I'
    STATUS = (
        (ACTIVO, 'Habilitado'),
        (INACTIVO, 'Inhabilitado'),
    )
    codigo_pensum = models.CharField(max_length=4, null=False, blank=False, unique=True, db_index=True,
                                     verbose_name='Código de pensum')
    descripcion = models.CharField(max_length=40, null=False, blank=False, verbose_name='Descripción del pensum')
    hora_semanal = models.IntegerField(null=False, blank=False, verbose_name='Horas semanal')
    estado = models.CharField(max_length=1, choices=STATUS, default='A')

    def __str__(self):
        return self.codigo_pensum+' / '+self.descripcion+', '+str(self.hora_semanal)+' Horas semanales'

    class Meta:
        db_table = 'tb_cde_pensum'
        verbose_name = 'Pensum'
        verbose_name_plural = 'Pensums'


class Pensum_detalle(models.Model):
    numero_unidad = models.IntegerField(null=False, blank=False, verbose_name='Unidad')
    actividad = models.CharField(max_length=40, null=False, blank=False, verbose_name='Actividad')
    numero_clase = models.IntegerField(null=False, blank=False, verbose_name='Número de clase')
    codigo_pensum = models.ForeignKey('Pensum', to_field='codigo_pensum', db_index=True, on_delete=models.CASCADE)

    class Meta:
        db_table = 'tb_cde_pensum_detalle'
        verbose_name = 'Pensum detalle'
        verbose_name_plural = 'Pensums detalle'

class Curso(models.Model):
    ACTIVO = 'A'
    INACTIVO = 'I'
    STATUS = (
        (ACTIVO, 'Habilitado'),
        (INACTIVO, 'Inhabilitado'),
    )
    codigo_curso = models.CharField(max_length=4, null=False, blank=False, unique=True, db_index=True,
                                    verbose_name='Código del curso')
    descripcion = models.CharField(max_length=40, null=False, blank=False, verbose_name='Nombre del curso')
    estado = models.CharField(max_length=1, choices=STATUS, default='A')
    id = models.AutoField(primary_key=True)
    concepto_inscripcion = models.ForeignKey(ConceptoFactura, to_field='codigo_concepto', on_delete=models.PROTECT, blank=True, null=True)

    def __str__(self):
        return self.codigo_curso+' / '+self.descripcion

    class Meta:
        db_table = 'tb_cde_curso'
        verbose_name = 'Curso'
        verbose_name_plural = 'Cursos'

class Nivel(models.Model):
    codigo_nivel = models.CharField(max_length=7, null=False, blank=False, unique=True, db_index=True,
                                    verbose_name='Código de nivel')
    descripcion = models.CharField(max_length=40, null=False, blank=False, verbose_name='Descripción del nivel')
    min_persona = models.IntegerField(null=False, blank=False, verbose_name='Mínimo para aperturar')
    meses_duracion = models.IntegerField(null=False, blank=False, verbose_name='Duración en meses')
    pensum = models.ManyToManyField(Pensum, verbose_name='Pensum aplicables para el nivel')
    monto_credito = models.DecimalField(max_digits=10, decimal_places=3, null=True, blank=True,)
    monto_contado = models.DecimalField(max_digits=10, decimal_places=3, null=True, blank=True,)
    excento_iva = models.BooleanField(default=False, verbose_name=_('Excento de I.V.A'))
    codigo_curso = models.ForeignKey(Curso,to_field='codigo_curso',  on_delete=models.CASCADE)

    def __str__(self):  # __unicode__ on Python 2
        return self.codigo_nivel

    class Meta:
        db_table = 'tb_cde_nivel'
        verbose_name = 'Nivel'
        verbose_name_plural = 'Niveles'

class Salon(models.Model):
    ACTIVO = 'A'
    INACTIVO = 'I'
    STATUS = (
        (ACTIVO, 'Habilitado'),
        (INACTIVO, 'Inhabilitado'),
    )
    codigo_salon = models.CharField(max_length=4, unique=True, verbose_name='Código del salon', db_index=True)
    nombre_salon = models.CharField(max_length=40, null=False, blank=False)
    capacidad = models.IntegerField(null=False, blank=False, verbose_name='Capacidad de alumnos', help_text='Es campo requerido')
    estado = models.CharField(max_length=1, choices=STATUS, default='A')

    def __str__(self):
        return self.codigo_salon+' / '+self.nombre_salon

    class Meta:
        db_table = 'tb_cde_salon'
        verbose_name = 'Salon'
        verbose_name_plural = 'Salones'


class Horario(models.Model):
    ACTIVO = 'A'
    INACTIVO = 'I'
    STATUS = (
        (ACTIVO, 'Habilitado'),
        (INACTIVO, 'Inhabilitado'),
    )

    codigo_horario = models.CharField(max_length=4, unique=True, db_index=True, verbose_name=_('Código horario'))
    descripcion = models.CharField(max_length=60, null=False, blank=False, verbose_name=_('Descripción'))
    salon = models.ManyToManyField(Salon)
    nivel = models.ManyToManyField(Nivel)
    estado = models.CharField(max_length=1, choices=STATUS, default='A')

    def __str__(self):
        return self.codigo_horario+' / '+self.descripcion

    class Meta:
        db_table = 'tb_cde_horario'
        verbose_name = 'Horario'
        verbose_name_plural = 'Horarios'


class Horario_detalle(models.Model):
    dia_semana = models.CharField(max_length=10, null=False, blank=False, verbose_name='Día de la semana')
    hora_inicio = models.CharField(max_length=10, null=False, blank=False, verbose_name='Hora de inicio')
    hora_culminacion = models.CharField(max_length=10, null=False, blank=False, verbose_name='Hora de culminación')
    codigo_horario = models.ForeignKey('Horario',to_field='codigo_horario', on_delete=models.CASCADE)

    class Meta:
        db_table = 'tb_cde_horario_detalle'
        verbose_name = 'Detalle horario'


class Profesor(models.Model):
    ACTIVO = 'A'
    INACTIVO = 'I'
    STATUS = (
        (ACTIVO, 'Habilitado'),
        (INACTIVO, 'Inhabilitado'),
    )
    cedula_profesor = models.CharField(max_length=10, null=False, blank=False, db_index=True,
                                       verbose_name='ID')
    nombre_profesor = models.CharField(max_length=60, null=False, blank=False, verbose_name='Nombre del Profesor')
    apellido_profesor = models.CharField(max_length=60, null=False, blank=False, verbose_name='Apellido del Profesor')
    direccion_habitacion = models.CharField(max_length=200, null=False, blank=False, verbose_name='Dirección')
    telefono = models.CharField(max_length=16, null=False, blank=False, verbose_name='Número de teléfono fijo')
    movil = models.CharField(max_length=16, null=False, blank=False, verbose_name='Número de teléfono movil')
    correo = models.CharField(max_length=60, null=False, blank=False, verbose_name='Correo electrónico')
    fecha_nacimiento = models.DateField(null=False, blank=False, verbose_name='Fecha de nacimiento')
    estado = models.CharField(max_length=1, choices=STATUS, default='A', verbose_name='Estado')
    id = models.AutoField(primary_key=True)

    class Meta:
        db_table = 'tb_cde_profesor'
        verbose_name = 'Profesor'
        verbose_name_plural = 'Profesores'


class Alumno(models.Model):
    ACTIVO = 'A'
    INACTIVO = 'I'
    STATUS = (
        (ACTIVO, 'Habilitado'),
        (INACTIVO, 'Inhabilitado'),
    )

    cedula_alumno = models.CharField(max_length=14, unique=True, null=False, blank=False, db_index=True, verbose_name='ID')
    nombre_alumno = models.CharField(max_length=60, null=False, blank=False, verbose_name='Nombre del Alumno')
    apellido_alumno = models.CharField(max_length=60, null=False, blank=False, verbose_name='Apellido del Alumno')
    direccion_habitacion = models.CharField(max_length=200, null=False, blank=False, verbose_name='Dirección')
    telefono = models.CharField(max_length=14, verbose_name='Número de teléfono fijo')
    telefono_opcional = models.CharField(max_length=14, null=True, blank=True,verbose_name='Número de teléfono opcional')
    movil = models.CharField(max_length=14, verbose_name='Número de teléfono movil')
    correo = models.CharField(max_length=60, null=False, blank=False, verbose_name='Correo electrónico')
    fecha_nacimiento = models.DateField(null=False, blank=False, verbose_name='Fecha de nacimiento')
    trabaja_guardia = models.BooleanField(default=False, verbose_name= 'Trabaja por guardia')
    estado = models.CharField(max_length=1, choices=STATUS, default='A')
    id = models.AutoField(primary_key=True)

    class Meta:
        db_table = 'tb_cde_alumno'
        verbose_name = 'Alumno'
        verbose_name_plural = 'Alumnos'
