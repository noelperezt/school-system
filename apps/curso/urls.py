from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from .views import *
from .transaccional.views import *
urlpatterns = [
    url(r'^salon/registrar/$', login_required(Salon_Registro.as_view()), name='salon_crear'),
    url(r'^salon/editar/(?P<pk>\d+)/$', login_required(Salon_Actualizar.as_view()), name='salon_editar'),
    url(r'^salon/listado', login_required(Salon_Listado.as_view()), name='salon_listado'),
    url(r'^salon/eliminar/(?P<pk>\d+)/$', login_required(Salon_Eliminar.as_view()), name='salon_eliminar'),

    url(r'^registrar/$', login_required(Curso_Crear.as_view()), name='curso_crear'),
    url(r'^editar/(?P<pk>\d+)/$', login_required(Curso_Actualizar.as_view()), name='curso_crear'),
    url(r'^listado', login_required(Curso_Listado.as_view()), name='curso_listado'),
    url(r'^eliminar/(?P<pk>\d+)/$', login_required(Curso_Eliminar.as_view()), name='curso_eliminar'),
    url(r'^catalogo-cursos/$', login_required(Catalogo_curso), name='curso_catalogo'),
    url(r'^datos-curso/$', login_required(DatosCurso), name='curso_datos'),

    url(r'^niveles/([0-9]+)/$', login_required(Curso_Crear_Niveles), name='niveles_crear'),
    url(r'^niveles/([0-9]+)/editar/([0-9]+)/$', login_required(Curso_Actualizar_Niveles), name='niveles_editar'),
    url(r'^niveles/(?P<r_pk>\d+)/eliminar/(?P<pk>\d+)/$', login_required(Curso_Eliminar_Niveles.as_view()), name='niveles_eliminar'),

    url(r'^profesor/listado', login_required(Profesor_Listado.as_view()), name='profesor_listado'),
    url(r'^profesor/registrar/$', login_required(Profesor_Crear.as_view()), name='profesor_crear'),
    url(r'^profesor/editar/(?P<pk>\d+)/$', login_required(Profesor_Actualizar.as_view()), name='profesor_editar'),
    url(r'^profesor/eliminar/(?P<pk>\d+)/$', login_required(Profesor_Eliminar.as_view()), name='profesor_eliminar'),

    url(r'^horario/listado', login_required(Horario_Listado.as_view()), name='horario_listado'),
    url(r'^horario/registrar/$', login_required(Horario_Crear), name='horario_crear'),
    url(r'^horario/editar/(?P<pk>\d+)/$', login_required(Horario_Actualizar.as_view()), name='horario_editar'),
    url(r'^horario/eliminar/(?P<pk>\d+)/$', login_required(Horario_Eliminar.as_view()), name='horario_eliminar'),
    url(r'^horario/(?P<r_pk>\d+)/eliminar/detalle/(?P<pk>\d+)/$', login_required(Horario_Eliminar_Detalle.as_view()), name='horario_detalle_eliminar'),

    url(r'^pensum/listado', login_required(Pensum_Listado.as_view()), name='pensum_listado'),
    url(r'^pensum/registrar/$', login_required(Pensum_Crear), name='pensum_crear'),
    url(r'^pensum/editar/(?P<pk>\d+)/$', login_required(Pensum_Actualizar.as_view()), name='pensum_editar'),
    url(r'^pensum/eliminar/(?P<pk>\d+)/$', login_required(Pensum_Eliminar.as_view()), name='pensum_eliminar'),
    url(r'^pensum/(?P<r_pk>\d+)/eliminar/detalle/(?P<pk>\d+)/$', login_required(Pensum_Eliminar_Detalle.as_view()), name='pensum_detalle_eliminar'),
    url(r'^pensum/eliminar/detalle/([0-9])/$', login_required(Pensum_Eliminar_Todo_Pensum), name='pensum_detalle_total_eliminar'),

    url(r'^oferta/registrar/$', login_required(OfertaCurso_Registrar), name='OfertaCurso_crear'),
    url(r'^oferta/editar/([0-9])/$', login_required(OfertaCurso_Actualizar), name='OfertaCurso_crear'),
    url(r'^oferta/listado', login_required(OfertaCursoListado.as_view()), name='OfertaCurso_listado'),

    url(r'^alumno/registrar/$', login_required(Alumno_Registrar.as_view()), name='alumno_crear'),

    url(r'^configuracion/$', login_required(Configuracion_Academico), name='academico_configuracion'),
]
