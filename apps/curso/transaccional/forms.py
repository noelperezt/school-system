from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib import messages
from .models import OfertaCurso
from ..models import Salon, Horario, Curso

class OfertarCursoForm(forms.ModelForm):
    class Meta:
        model = OfertaCurso
        fields = ['estado','cupos', 'nivel','pensum']

        widgets = {
            'cupos': forms.TextInput(attrs={'class': 'form-control', 'readonly':'readonly', 'value' : 0}),
        }

    def __init__(self, *args, **kwargs):
        super(OfertarCursoForm, self).__init__(*args, **kwargs)

        for campo in self.fields:
            self.fields[campo].widget.attrs.update({'class' : 'form-control','required' : 'required'})
