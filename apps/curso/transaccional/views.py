from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import CreateView, ListView, UpdateView, DeleteView
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import render
from django.contrib import messages
from django.db import connection, IntegrityError
from apps.sistema.util.Funciones import cursorDictFetchall
from .models import OfertaCurso
from ..models import Nivel, Pensum, Horario, Horario_detalle, Salon
from .forms import OfertarCursoForm
import json


def ajaxOfertaCurso(request):
    xHTML = '<option selected="selected" value="">---------</option>'
    if request.POST['accion'] == 'getNiveles':
        datos = Nivel.objects.filter(codigo_curso=request.POST['curso'])
        for nivel in datos:
            xHTML += '<option value = "' + str(
                nivel.codigo_nivel) + '">' + nivel.codigo_nivel + ' / ' + nivel.descripcion + '</option>'

    if request.POST['accion'] == 'getPensums':
        nivel = Nivel.objects.get(codigo_nivel=request.POST['nivel'])
        datos = Pensum.objects.filter(nivel__id = nivel.id, estado='A')
        for pensum in datos:
            xHTML += '<option value = "' + str(
                pensum.codigo_pensum) + '">' + pensum.codigo_pensum + ' / ' + pensum.descripcion + '</option>'

        ''' se busca la capacidad del salon seleccionado, la cual sera la cantidad de cupos disponibles
            a la venta para ese curss.
        '''

        return HttpResponse(json.dumps({'datos': xHTML,'cupos':nivel.min_persona}), content_type="application/json")

    if request.POST['accion'] == 'getDetalleHorario':
        datos = Horario_detalle.objects.filter(codigo_horario=request.POST['horario'])
        xHTML = ''
        for horario in datos:
            xHTML += '<tr><td>' + horario.dia_semana + '</td><td> ' + horario.hora_inicio + '</td><td> ' + horario.hora_culminacion + '</option>'

    return HttpResponse(json.dumps({'datos': xHTML}), content_type="application/json")


def OfertaCurso_Registrar(request):
    if request.is_ajax():
        return ajaxOfertaCurso(request)
    if request.method == 'POST':
        form = OfertarCursoForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/curso/oferta/listado')
        else:
            messages.add_message(request, messages.WARNING, form.errors)

    context = {
        'titulo': _('Ofertar cursos'),
        'form': OfertarCursoForm,
        'accion': ''
    }
    return render(request, 'curso/transaccion/ofertar_curso.html', context)

def OfertaCurso_Actualizar(request, pk_oferta):
    if request.method == 'POST':
        form = OfertarCursoForm(request.POST, instance=OfertaCurso.objects.get(pk=pk_oferta))
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/curso/oferta/listado')
        else:
            messages.add_message(request, messages.WARNING, form.errors)

    cursor = connection.cursor()
    cursor.execute("SELECT a.id, a.nivel_id, pensum_id, c.codigo_curso, c.descripcion, a.estado, cupos "
                   "FROM tb_cde_oferta_curso a, tb_cde_nivel b, tb_cde_curso c "
                   "WHERE a.nivel_id = b.codigo_nivel and b.codigo_curso_id = c.codigo_curso "
                   "       and a.id =  %s ",
                   [int(pk_oferta)]
                   )
    datos = cursorDictFetchall(cursor)


    data_niveles = Nivel.objects.filter(codigo_curso=datos[0]['codigo_curso'])
    data_pensum = Pensum.objects.filter(codigo_pensum=datos[0]['pensum_id'])
    context = {
        'datos' : datos[0],
        'form' : OfertarCursoForm,
        'niveles' : data_niveles,
        'pensum' : data_pensum,
        'accion': 'actualizar',
    }
    return render(request, 'curso/transaccion/ofertar_curso.html', context)


class OfertaCursoListado(ListView):
    model = OfertaCurso
    template_name = 'curso/transaccion/ofertar_curso_listado.html'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(OfertaCursoListado, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Listado de cursos ofertados')

        return context

    #def get_queryset(self):
    #    pass
