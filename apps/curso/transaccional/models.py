from django.db import models
from django.utils.translation import ugettext_lazy as _
from apps.curso.models import Salon, Nivel, Horario, Pensum, Alumno, Profesor, Pensum_detalle

class OfertaCurso(models.Model):
    INSCRIPCION = 'I'
    SUSPENDIDO = 'S'
    FINALIZADO = 'F'
    PROCESO_CLASE = 'P'
    CUPOS_COMPLETADOS = 'C'
    STATUS = (
        (INSCRIPCION, 'Inscripcion'),
        (SUSPENDIDO, 'Suspendido'),
        (FINALIZADO, 'Finalizado'),
        (PROCESO_CLASE, 'Proceso de clases'),
        (CUPOS_COMPLETADOS, 'Cupos completados'),
    )
    cupos = models.IntegerField(verbose_name= _('Cantidad de cupos'), blank=True, null=True)
    edad_minima = models.IntegerField(verbose_name= _('Edad mínima permitida'), blank=True, null=True)
    edad_max = models.IntegerField(verbose_name= _('Edad máxima permitida'), blank=True, null=True)
    estado = models.CharField(max_length=1, db_index= True, choices=STATUS, default='I')
    cierre_inscripcion = models.DateField(null=True, blank=True)
    salon = models.ForeignKey(Salon, to_field= 'codigo_salon',null=True, blank=True, on_delete=models.PROTECT)
    profesor = models.ForeignKey(Profesor, on_delete=models.PROTECT, blank=True, null=True)
    nivel = models.ForeignKey(Nivel, to_field='codigo_nivel', db_index=True, on_delete=models.PROTECT)
    horario = models.ForeignKey(Horario, to_field='codigo_horario', db_index= True, null=True, blank=True, on_delete=models.PROTECT)
    pensum = models.ForeignKey(Pensum, to_field ='codigo_pensum', on_delete=models.PROTECT)

    class Meta:
        db_table = 'tb_cde_oferta_curso'
        verbose_name = 'Oferta de curso'
        verbose_name_plural = 'Cursos ofertados'


class Inscripcion(models.Model):
    Aprovado = 'A'
    Repobado = 'R'
    Desertor = 'D'
    En_Curso = 'C'
    solo_contrato = 'T'
    Congelado = 'G'
    Anulado = 'X' #si el contrato va por su segundo nivel no se puede anular 'ojo'

    STATUS = (
        (Aprovado, 'Aprovado'),
        (Repobado, 'Suspendido'),
        (Desertor, 'Finalizado'),
        (En_Curso, 'En curso'),
        (solo_contrato, 'Temporal solo hizo contrato'),
        (Congelado,'Congelado'),
        (Anulado, 'Anulado'),
    )
    alumno = models.ForeignKey(Alumno, db_index= True)
    nivel_siguiente = models.ForeignKey(Nivel, on_delete=models.PROTECT)
    oferta = models.ForeignKey(OfertaCurso, db_index=True)
    fecha_inicio = models.DateField(verbose_name= _('Fecha de inicio de clases'))
    fecha_culminacion = models.DateField(verbose_name= _('Fecha de culminación de clases'))
    estado = models.CharField(max_length=1, db_index=True, choices=STATUS, default='T')
    genera_financiamiento = models.CharField

    class Meta:
        db_table = 'tb_cde_inscripcion'
        verbose_name = 'Inscripcion'
        verbose_name_plural = 'Inscripciones'


class Cronograma(models.Model):
    fecha_clase = models.DateField(null=False, blank=False)
    pensum_detalle = models.ForeignKey(Pensum_detalle, on_delete=models.PROTECT)
    oferta = models.ForeignKey(OfertaCurso, on_delete=models.CASCADE)

    class Meta:
        db_table = 'tb_cde_cronograma'
        verbose_name = 'Cronograma actividad'
        verbose_name_plural = 'Cronogramas de Actividades'