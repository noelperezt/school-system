from django import forms
from .models import *


class SalonForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['estado'].widget.attrs.update({'class': 'form-control'})

    estado = forms.ChoiceField( choices=Salon.STATUS)

    class Meta:
        model = Salon
        fields = ['codigo_salon', 'nombre_salon', 'estado', 'capacidad']

        widgets = {

            'codigo_salon': forms.TextInput(attrs={'required': 'required','class': 'form-control', 'autofocus' : 'on'}),
            'nombre_salon': forms.TextInput(attrs={'required': 'required','class': 'form-control'}),
            'capacidad': forms.NumberInput(attrs={'required': 'required','class': 'form-control'}),
        }

class CursoForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['estado'].widget.attrs.update({'class': 'form-control'})

    estado = forms.ChoiceField(choices=Salon.STATUS)

    class Meta:
        model = Curso
        fields = ['codigo_curso', 'descripcion', 'estado']

        widgets = {

            'codigo_curso': forms.TextInput(attrs={'required': 'required', 'class': 'form-control',
                                                   'autofocus': 'on'}),
            'descripcion': forms.TextInput(attrs={'required': 'required', 'class': 'form-control'}),
        }

class NivelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['pensum'].widget.attrs.update({'class': 'form-control'})
    class Meta:
        model = Nivel
        fields = ['codigo_nivel', 'descripcion', 'min_persona','meses_duracion','codigo_curso','pensum']

        widgets = {
            'codigo_nivel': forms.TextInput(attrs={'required': 'required', 'class': 'form-control','autofocus': 'on'}),
            'descripcion': forms.TextInput(attrs={'required': 'required', 'class': 'form-control'}),
            'min_persona': forms.NumberInput(attrs={'required': 'required', 'class': 'form-control'}),
            'meses_duracion': forms.NumberInput(attrs={'required': 'required', 'class': 'form-control'}),

        }

class ProfesorForm(forms.ModelForm):
    INPUT_FORMAT = (
        ('%d/%m/%Y','%m/%d/%Y')
    )
    fecha_nacimiento = forms.DateField(input_formats = INPUT_FORMAT)
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['estado'].widget.attrs.update({'class': 'form-control'})
        self.fields['fecha_nacimiento'].widget.attrs.update({'required': 'required', 'class': 'form-control', 'id' : 'single_cal2'})

    estado = forms.ChoiceField(choices=Profesor.STATUS)
    class Meta:
        model = Profesor
        fields = ['cedula_profesor', 'nombre_profesor', 'apellido_profesor', 'direccion_habitacion', 'telefono'
                    ,'movil', 'correo', 'fecha_nacimiento', 'estado']

        widgets = {
            'cedula_profesor': forms.NumberInput(attrs={'required': 'required', 'class': 'form-control',
                                                      'autofocus': 'on','maxlenght' : '10'}),
            'nombre_profesor': forms.TextInput(attrs={'required': 'required', 'class': 'form-control'}),
            'apellido_profesor': forms.TextInput(attrs={'required': 'required', 'class': 'form-control'}),
            'direccion_habitacion': forms.TextInput(attrs={'required': 'required', 'class': 'form-control',
                                                           'maxlenght' : '200'}),
            'telefono': forms.TextInput(attrs={'required': 'required', 'class': 'form-control'}),
            'movil': forms.TextInput(attrs={'required': 'required', 'class': 'form-control'}),
            'correo': forms.EmailInput(attrs={'required': 'required', 'class': 'form-control'}),

        }

class HorarioForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for campo in self.fields:
            self.fields[campo].widget.attrs.update({'class' : 'form-control'})
            self.fields[campo].widget.attrs.update({'required' : 'required'})

    estado = forms.ChoiceField(choices=Horario.STATUS)
    class Meta:
        model = Horario
        fields = ['codigo_horario', 'descripcion', 'estado','id']

class PensumForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for campo in self.fields:
            self.fields[campo].widget.attrs.update({'class' : 'form-control'})
            self.fields[campo].widget.attrs.update({'required' : 'required'})
    estado = forms.ChoiceField(choices=Pensum.STATUS)

    class Meta:
        model = Pensum
        fields = ['codigo_pensum', 'hora_semanal', 'estado','descripcion']


class AlumnoForm(forms.ModelForm):
    INPUT_FORMAT = (
        ('%d/%m/%Y','%m/%d/%Y')
    )
    fecha_nacimiento = forms.DateField(input_formats = INPUT_FORMAT)
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for campo in self.fields:
            if campo != 'estado' or campo != 'trabaja_guardia':
                self.fields[campo].widget.attrs.update({'class' : 'form-control'})
                self.fields[campo].widget.attrs.update({'required' : 'required'})

        self.fields['trabaja_guardia'].widget.attrs.update({'class' : 'check'})

    class Meta:
        model = Alumno
        fields = '__all__'

class CongiguracionForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for campo in self.fields:
                self.fields[campo].widget.attrs.update({'class' : 'form-control'})
                self.fields[campo].widget.attrs.update({'required' : 'required'})


    class Meta:
        model = Configuracion
        fields = ['oferta_salon_culminar_clases', 'extiende_inscripcion', 'configuracio_aplicada']