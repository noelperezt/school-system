from django.db import models
from django.utils.translation import ugettext_lazy as _
from apps.sistema.models import Usuario
# Create your models here.

class Vendedor(models.Model):
    ACTIVO = 'A'
    INACTIVO = 'I'
    STATUS = (
        (ACTIVO, _('Habilitado')),
        (INACTIVO, _('Inhabilitado')),
    )   
    movil = models.CharField(max_length=20,null=False, blank=False,verbose_name=_('Número de teléfono movil'))
    correo = models.CharField(max_length=60, null=False, blank=False, verbose_name=_('Correo electrónico'))
    fecha_nacimiento = models.DateField(null=True, blank=True, verbose_name=_('Fecha de nacimiento'))
    direccion_habitacion = models.CharField(max_length=200, null=False, blank=False, verbose_name=_('Dirección'))
    estado = models.CharField(max_length=1, choices=STATUS, default='A')
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    class Meta:
        db_table = 'tb_vnt_vendedor'
        verbose_name = 'Vendedor'
        verbose_name_plural = 'Vendedores'



