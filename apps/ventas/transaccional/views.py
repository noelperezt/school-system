from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse_lazy
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse
from django.db import connection, IntegrityError
from django.db.models import Q
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
from .forms import VentaContratoForm
from .models import Contrato, Vendedor
from apps.administracion.forms import ClienteForm
from apps.administracion.models import Cliente, ConceptoFactura
from apps.curso.forms import AlumnoForm
from apps.curso.models import Nivel, Horario, Curso
from apps.curso.transaccional.models import Inscripcion
from apps.curso.models import Alumno
from apps.sistema.util.Funciones import cursorDictFetchall
from apps.administracion.models import Empresa
from datetime import datetime
from apps.curso.clases.Inscripcion import Class_Inscripcion
from apps.curso.transaccional.models import OfertaCurso
import json
import re

class Venta_Contrato(CreateView):
    model = Contrato
    form_class = VentaContratoForm
    form_cliente = ClienteForm
    form_alumno = AlumnoForm
    success_url = reverse_lazy('ventas:vendedor_listado')
    template_name = 'ventas/transaccion/contrato_venta.html'

    def get_context_data(self, **kwargs):
        context = super(Venta_Contrato, self).get_context_data(**kwargs)
        vendedor = 0
        try:
            datos = Vendedor.objects.get(usuario=self.request.user.id)
            vendedor = datos.id
        except ObjectDoesNotExist:
            pass


        if 'vendedor' not in context:
            context['vendedor'] = vendedor
        if 'titulo' not in context:
            context['titulo'] = _('Venta de contrato de servicio')

        if 'form1' not in context:
            context['form1'] = self.form_class
        if 'form2' not in context:
            context['form2'] = self.form_cliente
        if 'form3' not in context:
            context['form3'] = self.form_alumno
        return context


    def post(self, request, *args, **kwargs):
        self.object = self.get_object

        try:
            ''' hacer inscripcion '''

            inscripcion = Class_Inscripcion
            inscripcion.setInscribir(request)

            '''   ***********    '''

            form = self.form_class(request.POST)
            if form.is_valid():
                nuevo_contrato = form.save(commit=False)
                return HttpResponseRedirect(self.success_url)
        except IntegrityError as e:
            return self.render_to_response(self.get_context_data(form=form))

    def form_invalid(self, form):
        if not form.is_valid():
            messages.add_message(self.request, messages.WARNING, form.errors)
        return self.render_to_response(self.get_context_data(form=form))

@csrf_exempt
def Buscar_Datos_Contrato(request):
    xHTML = ''
    mensaje = ''
    if request.is_ajax():
        if request.POST:
            if request.POST['accion'] == 'getNiveles':
                curso = Curso.objects.get(id=request.POST['curso'])

                '''datos = Nivel.objects.select_related().filter(
                    codigo_nivel__in=OfertaCurso.objects.filter(estado='A').values('nivel'),
                    codigo_curso=curso.codigo_curso
                )'''
                datos = Nivel.objects.filter(
                    codigo_nivel__in=OfertaCurso.objects.filter(estado='I', cupos__gt=0).values('nivel'),
                    codigo_curso=curso.codigo_curso
                )
                curso_inscripcion = str(curso.concepto_inscripcion).split('/')

                datos_cursos = ConceptoFactura.objects.get(codigo_concepto=curso_inscripcion[0].strip())
                monto_inscripcion = str(round(datos_cursos.monto_neto,2))

                if not datos:
                    mensaje = _('El curso seleccionado no posee ofertas con cupos disponibles')

                    ''' si no consigue ofertas con cupos disponibles, se verifica que haya curso que iniciaron pero
                     que el salon tiene disponiblidad para ingresar mas personas '''
                else:                    
                    for niveles in datos:
                        xHTML += '<tr>'
                        xHTML += '<td><input type = "checkbox" class = "flat-red niveles" name = "niveles[]" value= "' +str(niveles.id) + '" ></td>'
                        xHTML += '<td>' + niveles.codigo_nivel + '</td>'
                        xHTML += '<td>' + niveles.descripcion + '</td>'
                        xHTML += '<td>' + str(niveles.meses_duracion) + ' Meses</td>'
                        xHTML += '</tr>'
                return HttpResponse(json.dumps({'datos': xHTML, 'mensaje': str(mensaje), 'inscripcion' : monto_inscripcion}),
                                    content_type="application/json")
            if request.POST['accion']  == 'getHorario':
                cursor = connection.cursor()
                cursor.execute("select a.id, codigo_horario, descripcion, cupos, edad_minima, edad_max, b.id as oferta "
                               "from tb_cde_horario a, tb_cde_oferta_curso b "
                               "where a.id = b.horario_id and nivel_id = %s "
                               "and a.id in (select horario_id from tb_cde_horario_nivel where nivel_id = %s) ",
                               [int(request.POST['nivel']), int(request.POST['nivel'])]
                )
                datos = cursorDictFetchall(cursor)
                if not datos:
                    mensaje = _('Disculpe el primer nivel seleccionado no posee horarios ofertados')

                for horario in datos:
                        
                    xHTML += '<tr>'
                    xHTML += '<td><input type = "checkbox" class = "flat-red horario" name = "horario" value= "' + str(horario['id']) + '" '
                    xHTML += 'edad_min = "'+str(horario['edad_minima']).replace('None','')+'" oferta = "'+str(horario['oferta'])+'" '
                    xHTML += 'edad_max = "'+str(horario['edad_max']).replace('None','')+'"></td>'
                    xHTML += '<td>' + horario['codigo_horario'] + '</td>'
                    xHTML += '<td>' + horario['descripcion'] + '</td>'
                    xHTML += '<td>' + str(horario['cupos']) + '</td>'
                    xHTML += '<td>' + str(horario['edad_minima']).replace('None','') + ' - '+ str(horario['edad_max']).replace('None','') +'</td>'
                    xHTML += '</tr>'
            
            if request.POST['accion'] == 'getClientes':
                if 'frase' in request.POST and len(request.POST['frase']) > 0:
                    Clientes = Cliente.objects.filter(estado='A', razon_social__contains=request.POST['frase'])                    
                else:
                    Clientes = Cliente.objects.filter(estado='A')

                paginator = Paginator(Clientes, 10)
                
                try:
                    page = request.POST['pagina']
                    datos = paginator.page(page)                    
                except PageNotAnInteger:
                    datos = paginator.page(1)
                except EmptyPage:
                    # If page is out of range (e.g. 9999), deliver last page of results.
                    datos = paginator.page(paginator.num_pages)
                paginas = datos.previous_page_number
                
                #return HttpResponse(serializers.serialize('json', datos, fields=('rif_cliente','razon_social')), content_type="application/json")
                for cliente in datos:
                    xHTML += '<tr>'
                    xHTML += '<td><a style = "cursor:pointer" onclick="seleccion_cliente(\''+cliente.rif_cliente+'\',\''+str(cliente.razon_social)+'\')">'+cliente.rif_cliente+'</a></td>'
                    xHTML += '<td>' + cliente.razon_social + '</td>'
                    xHTML += '</tr>'
                return HttpResponse(json.dumps({'datos': xHTML,'mensaje': str(mensaje),'pagina':str(paginas)}), content_type="application/json")

            if request.POST['accion'] == 'getDatoCliente':
                try:
                    datos = Cliente.objects.get(estado='A', rif_cliente=request.POST['rif_cliente'])
                    return HttpResponse(json.dumps({'razon_social':datos.razon_social,'mensaje' : '' }), content_type="application/json")
                except ObjectDoesNotExist:
                    pass

            if request.POST['accion'] == 'getAlumno':
                if 'frase' in request.POST and len(request.POST['frase']) > 0:
                    Alumnos = Alumno.objects.exclude(
                        id__in=Inscripcion.objects.filter(Q(estado='G') | Q(estado='T') | Q(estado='C') ).values('alumno')
                    ).filter(estado='A', nombre_alumno__contains=request.POST['frase'])
                else:
                    Alumnos = Alumno.objects.exclude(
                        id__in=Inscripcion.objects.filter(Q(estado='G') | Q(estado='T') | Q(estado='C') ).values('alumno')
                    ).filter(estado='A')
                paginator = Paginator(Alumnos, 10)
                
                try:
                    page = request.POST['pagina']
                    datos = paginator.page(page)                    
                except PageNotAnInteger:
                    datos = paginator.page(1)
                except EmptyPage:
                    # If page is out of range (e.g. 9999), deliver last page of results.
                    datos = paginator.page(paginator.num_pages)
                paginas = datos.previous_page_number

                for alumno in datos:
                    xHTML += '<tr>'
                    xHTML += '<td><a style = "cursor:pointer" onclick="'
                    xHTML += 'seleccion_alumno(\''+alumno.cedula_alumno+'\',\''+str(alumno.nombre_alumno)+'\',\''+str(alumno.apellido_alumno)+'\')">'+alumno.cedula_alumno+'</a></td>'
                    xHTML += '<td>' + alumno.nombre_alumno + '</td>'
                    xHTML += '<td>' + alumno.apellido_alumno + '</td>'
                    xHTML += '</tr>'
                return HttpResponse(json.dumps({'datos': xHTML,'mensaje': str(mensaje),'pagina':str(paginas)}), content_type="application/json")


            if request.POST['accion'] == 'getDatosAlumno':
                try:
                    datos = Alumno.objects.get(cedula_alumno=request.POST['cedula'])
                    fecha = datetime.strptime(str(datos.fecha_nacimiento), "%Y-%m-%d")
                    return HttpResponse(json.dumps({'fecha_nacimiento':fecha.strftime('%d/%m/%Y'), 'nombre':datos.nombre_alumno,'apellido':datos.apellido_alumno,'mensaje' : '' }), content_type="application/json")
                except ObjectDoesNotExist:
                    return HttpResponse(json.dumps(
                        { 'mensaje': _('Alumno no se encuentra registrado')}), content_type="application/json")
                    
            if request.POST['accion'] == 'getMontoNiveles':
                array_niveles = request.POST['niveles'].split(',');
                empreas = Empresa.objects.all()
                tasa_iva = empreas[0].tasa_iva
                

                #limpio los id enviados
                i = 0
                for nivel in array_niveles:
                    array_niveles[i] = nivel.replace('[','').replace('"','').replace("]",'')
                    i = i + 1

                total_monto_credito = 0
                total_monto_contado = 0
                cantidad_cuotas = 0
                for nivel in array_niveles:
                    datos_nivel = Nivel.objects.get(id=nivel)                    
                    total_monto_credito = float(total_monto_credito) + float(datos_nivel.monto_credito)
                    total_monto_contado = float(total_monto_contado) + float(datos_nivel.monto_contado)
                    cantidad_cuotas = cantidad_cuotas  + int(datos_nivel.meses_duracion)
                    xHTML += '<tr>'
                    xHTML += '<td>'+  str(datos_nivel.codigo_nivel) +'</td>'                    
                    xHTML += '<td>'+ datos_nivel.descripcion +'</td>'                    
                    xHTML += '<td style = "text-align: center;">'+ str(datos_nivel.monto_credito) +'</td>'                    
                    xHTML += '<td style = "text-align: center;">'+ str(datos_nivel.monto_contado) +'</td>'                    
                    xHTML += '</tr>'

                return HttpResponse(json.dumps({'datos': xHTML,'mensaje': '','totales':{'credito': str(total_monto_credito),
                    'contado': str(total_monto_contado)}, 'iva' : str(tasa_iva), 'cuotas' : cantidad_cuotas}), content_type="application/json")
                    
            return HttpResponse(json.dumps({'datos': xHTML,'mensaje': str(mensaje)}), content_type="application/json")

#def Venta_Contrato(request):

    #context = {
    #    'titulo' : 'Registro de contratos',
    #    'form' : VentaContratoForm
    #}
    #return render(request, 'ventas/transaccion/contrato_venta.html', context)
class ResponseAjax(object):
    def form_invalid(self, form):
        to_json = {}
        to_json.update(success=False)
        if form.errors:
            errors = {}
            fields = {}
            for field_name, text in form.errors.items():
                fields[field_name] = text

            errors.update(fields=fields)
            to_json.update(errors=errors)
        return HttpResponse(json.dumps(to_json), content_type="application/json")

    def form_valid(self, form):
        self.object = form.save()
        context = {'success': True}
        return HttpResponse(json.dumps(context), content_type="application/json")



class Registrar_Cliente(ResponseAjax, CreateView):
    models = Cliente
    form_class = ClienteForm
    def get_context_data(self, context):
        context['success'] = True
        return context