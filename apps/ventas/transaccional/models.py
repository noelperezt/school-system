from django.db import models
from django.utils import timezone
from apps.curso.models import Curso, Nivel, Alumno
from ..models import Vendedor
from apps.administracion.models import Cliente
from django.utils.translation import ugettext_lazy as _

class Contrato(models.Model):
    ACTIVO = 'A'
    ANULADO = 'X'
    EXONERADO = 'E'
    CONGELADO = 'C'
    EXAMEN_NIVELACION = 'N'
    RESERVADO = 'R'
    STATUS = (
        (ACTIVO, 'Activo'),
        (ANULADO, 'Anulado'),
        (EXONERADO, 'Exonerado'),
        (CONGELADO, 'Congelado'),
        (EXAMEN_NIVELACION, 'Examen de nivelación'),
        (RESERVADO, 'Reservado'),
    )
    numero_contrato = models.AutoField(primary_key=True)
    monto_inscripcion = models.DecimalField(max_digits=3, decimal_places=1, null=False, blank=False,
                                            verbose_name='Monto inscripción', default=0)
    monto_contrato = models.DecimalField(max_digits=3, decimal_places=1, null=False, blank=False,
                                         verbose_name='Monto del ventas')
    monto_cuota = models.DecimalField(max_digits=3, decimal_places=1, null=False, blank=False,
                                      verbose_name='Monto de la cuota', default=0)
    tasa_iva = models.DecimalField(max_digits=2, decimal_places=1, null=False, blank=False, verbose_name=_('Tasa de iva'))    
    duracion_curso = models.IntegerField(null=False, blank=False, verbose_name=_('Duración del curso'))
    fecha_contrato = models.DateTimeField(default=timezone.now, verbose_name=_('Fecha de registro del ventas'))
    fecha_anulado = models.DateTimeField(null=True, blank=True, verbose_name=_('Fecha que se anuló o congeló el ventas'))
    aplica_descuento_referido = models.CharField(max_length=1, null=False, blank=False, default='N',
                                                 verbose_name=_('Aplica descuento por referido'))
    estado = models.CharField(max_length=1, default='A')
    pagado = models.CharField(max_length=1, default='N')
    rif_cliente = models.ForeignKey(Cliente,db_column='rif_cliente', db_index=True, on_delete=models.PROTECT)
    cedula_alumno = models.ForeignKey(Alumno,db_column='cedula_alumno', db_index=True, on_delete=models.PROTECT)
    cedula_referido = models.CharField(max_length=14, null=True, blank=True, verbose_name=_('ID Referido'))
    vendedor = models.ForeignKey(Vendedor, db_index=True, on_delete=models.PROTECT)
    codigo_curso = models.ForeignKey(Curso, db_index=True, on_delete=models.PROTECT)

    class Meta:
        db_table = 'tb_vnt_contrato'
        verbose_name = 'Contrato'
        verbose_name_plural = 'Contratos'


class Contrato_detalle(models.Model):
    numero_contrato = models.ForeignKey(Contrato, db_index=True, on_delete=models.CASCADE)
    codigo_nivel = models.ForeignKey(Nivel, db_index=True, on_delete=models.PROTECT)

    class Meta:
        db_table = 'tb_vnt_contrato_detalle'