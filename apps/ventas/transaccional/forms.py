from django import forms
from .models import Contrato
from apps.administracion.models import Empresa


class VentaContratoForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for campo in self.fields:
            self.fields[campo].widget.attrs.update({'class': 'form-control', 'required': 'required'})

    class Meta:
        model = Contrato
        fields = ['monto_inscripcion', 'monto_contrato', 'tasa_iva', 'duracion_curso',
                  'aplica_descuento_referido', 'rif_cliente', 'cedula_alumno', 'cedula_referido', 'vendedor',
                  'codigo_curso']

        widgets = {
            'aplica_descuento_referido': forms.HiddenInput(attrs={'required': 'required'}),
            'duracion_curso': forms.HiddenInput(attrs={'required': 'required'}),
        }