from django.shortcuts import render
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from .models import Vendedor
from apps.sistema.models import Usuario
from .forms import VendedorForm


class Vendedor_Crear(CreateView):
    model = Vendedor
    form_class = VendedorForm
    success_url = reverse_lazy('ventas:vendedor_listado')
    template_name = 'ventas/vendedor.html'
    def get_context_data(self, **kwargs):
        context = super(Vendedor_Crear, self).get_context_data(**kwargs)       
        if 'titulo' not in context:
            context['titulo'] = _('Registrar ejecutivos de venta')

        return context

    def form_invalid(self, form):        
        if not form.is_valid():
            print(form.errors)
            messages.add_message(self.request, messages.WARNING, form.errors)
        return self.render_to_response(self.get_context_data(form=form))

class Vendedor_Actualizar(UpdateView):
    model = Vendedor
    form_class = VendedorForm
    success_url = reverse_lazy('ventas:vendedor_listado')
    template_name = 'ventas/vendedor.html'

    def get_context_data(self, **kwargs):
        context = super(Vendedor_Actualizar, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Actualizar datos del ejecutivos de venta')

        if 'accion' not in context:
            context['accion'] = 'actualizar'

        return context

    def form_invalid(self, form):        
        if not form.is_valid():
            messages.add_message(self.request, messages.WARNING, form.errors)
        return self.render_to_response(self.get_context_data(form=form))

class Vendedor_Eliminar(DeleteView):
    model = Vendedor
    template_name = 'ventas/vendedor_eliminar.html'
    success_url = reverse_lazy('ventas:vendedor_listado')

    def get_context_data(self, **kwargs):
        context = super(Vendedor_Eliminar, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Eliminar ejecutivo de ventas')

        return context

    def form_invalid(self, form):        
        if not form.is_valid():
            messages.add_message(self.request, messages.WARNING, form.errors)
        return self.render_to_response(self.get_context_data(form=form))

class Vendedor_Listado(ListView):
    model = Vendedor
    template_name = 'ventas/vendedor_listado.html'
    paginate_by = 10
    def get_context_data(self, **kwargs):
        context = super(Vendedor_Listado, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Listado de ejecutivos de venta registrados en el sistema')

        return context

    def get_queryset(self):
        #print(self.request.GET)
        query = Vendedor.objects.filter(estado='A').select_related("usuario")        
        return query

