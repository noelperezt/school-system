from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from .views import *
from .transaccional.views import  *

urlpatterns = [
    url(r'^ejecutivo-venta/listado', login_required(Vendedor_Listado.as_view()), name='vendedor_listado'),
    url(r'^ejecutivo-venta/registrar/$', login_required(Vendedor_Crear.as_view()), name='vendedor_crear'),
    url(r'^ejecutivo-venta/editar/(?P<pk>\d+)/$', login_required(Vendedor_Actualizar.as_view()), name='vendedor_editar'),
    url(r'^ejecutivo-venta/eliminar/(?P<pk>\d+)/$', login_required(Vendedor_Eliminar.as_view()), name='vendedor_eliminar'),

    url(r'^contrato/$', login_required(Venta_Contrato.as_view()), name='contrato_venta'),
    url(r'^contrato/buscar-datos/$', login_required(Buscar_Datos_Contrato), name='buscar_datos_contrato'),
    url(r'^contrato/registrar-cliente/$', login_required(Registrar_Cliente.as_view()), name='cliente_crear'),
]