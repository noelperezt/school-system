# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-11-19 16:35
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('administracion', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('curso', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contrato',
            fields=[
                ('numero_contrato', models.AutoField(primary_key=True, serialize=False)),
                ('monto_inscripcion', models.DecimalField(decimal_places=1, default=0, max_digits=3, verbose_name='Monto inscripción')),
                ('monto_contrato', models.DecimalField(decimal_places=1, max_digits=3, verbose_name='Monto del ventas')),
                ('monto_cuota', models.DecimalField(decimal_places=1, default=0, max_digits=3, verbose_name='Monto de la cuota')),
                ('tasa_iva', models.DecimalField(decimal_places=1, max_digits=2, verbose_name='Tasa de iva')),
                ('duracion_curso', models.IntegerField(verbose_name='Duración del curso')),
                ('fecha_contrato', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Fecha de registro del ventas')),
                ('fecha_anulado', models.DateTimeField(blank=True, null=True, verbose_name='Fecha que se anuló o congeló el ventas')),
                ('aplica_descuento_referido', models.CharField(default='N', max_length=1, verbose_name='Aplica descuento por referido')),
                ('estado', models.CharField(default='A', max_length=1)),
                ('pagado', models.CharField(default='N', max_length=1)),
                ('cedula_referido', models.CharField(blank=True, max_length=14, null=True, verbose_name='ID Referido')),
                ('cedula_alumno', models.ForeignKey(db_column='cedula_alumno', on_delete=django.db.models.deletion.PROTECT, to='curso.Alumno')),
                ('codigo_curso', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='curso.Curso')),
                ('rif_cliente', models.ForeignKey(db_column='rif_cliente', on_delete=django.db.models.deletion.PROTECT, to='administracion.Cliente')),
            ],
            options={
                'db_table': 'tb_vnt_contrato',
                'verbose_name_plural': 'Contratos',
                'verbose_name': 'Contrato',
            },
        ),
        migrations.CreateModel(
            name='Contrato_detalle',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigo_nivel', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='curso.Nivel')),
                ('numero_contrato', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ventas.Contrato')),
            ],
            options={
                'db_table': 'tb_vnt_contrato_detalle',
            },
        ),
        migrations.CreateModel(
            name='Vendedor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('movil', models.CharField(max_length=20, verbose_name='Número de teléfono movil')),
                ('correo', models.CharField(max_length=60, verbose_name='Correo electrónico')),
                ('fecha_nacimiento', models.DateField(blank=True, null=True, verbose_name='Fecha de nacimiento')),
                ('direccion_habitacion', models.CharField(max_length=200, verbose_name='Dirección')),
                ('estado', models.CharField(choices=[('A', 'Habilitado'), ('I', 'Inhabilitado')], default='A', max_length=1)),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'tb_vnt_vendedor',
                'verbose_name_plural': 'Vendedores',
                'verbose_name': 'Vendedor',
            },
        ),
        migrations.AddField(
            model_name='contrato',
            name='vendedor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='ventas.Vendedor'),
        ),
    ]
