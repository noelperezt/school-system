from django import forms
from .models import Vendedor
from django.utils.translation import ugettext_lazy as _

class VendedorForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['estado'].widget.attrs.update({'class': 'form-control'})

        self.fields['usuario'].widget.attrs.update({'class': 'form-control'})

    estado = forms.ChoiceField(choices=Vendedor.STATUS)
    

    class Meta:
        model = Vendedor
        fields = [ 'direccion_habitacion','usuario'
                    ,'movil', 'correo', 'fecha_nacimiento', 'estado']

        widgets = {
            'direccion_habitacion': forms.TextInput(attrs={'required': 'required', 'class': 'form-control',
                                                           'maxlenght' : '200'}),
            'movil': forms.TextInput(attrs={'required': 'required', 'class': 'form-control'}),
            'correo': forms.EmailInput(attrs={'required': 'required', 'class': 'form-control'}),
            'fecha_nacimiento' : forms.TextInput(attrs={'required': 'required', 'class': 'form-control', 'id' : 'single_cal2'}),
        }