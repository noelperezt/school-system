from django.shortcuts import render, redirect
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.db import IntegrityError, transaction
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse_lazy
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.utils.translation import ugettext_lazy as _
from django.views.generic import CreateView, ListView, DeleteView, UpdateView
from apps.curso.models import Curso, Nivel
from .models import Empresa, ConceptoFactura, Cliente, Departamento
from .forms import ConceptoFacturaForm, CostoCursoForm, ClienteForm, EmpresaForm, DepartamentoForm
from apps.sistema.util.File import File
import json

def Subir_logo(request):
    archivo = File(request.FILES['foto'], '', 'logo')
    if not archivo.upload():
        return HttpResponse(json.dumps({'mensaje': str(archivo.mensaje)}), content_type="application/json")


    return HttpResponse(json.dumps({'mensaje': str(archivo.mensaje)}), content_type="application/json")

def configuracion_empresa(request):
    mensaje = ''
    accion = 'grabar'
    if request.method == 'POST':
        if request.POST['accion'] == 'grabar':
            accion = 'grabar'
            form = EmpresaForm(request.POST)
            if form.is_valid():
                file = File(request.FILES['logo_img'], '', 'logo')
                file.upload()

                form_insert = form.save(commit=False)
                form_insert.logo = str(file.getFile())
                form_insert.save()

                HttpResponseRedirect('/administrativo/configuracion/empresa/')
            else:
                messages.add_message(request, messages.WARNING, form.errors)
        else:
            accion = 'update'
            datos = Empresa.objects.all()
            rif = datos[0].codigo_empresa
            form = EmpresaForm(request.POST,instance=Empresa.objects.get(pk=rif))
            if form.is_valid():
                file = File(request.FILES['logo_img'], '', 'logo')
                file.upload()

                form_update = form.save(commit=False)
                form_update.logo = str(file.getFile())
                form_update.save()

                HttpResponseRedirect('/administrativo/configuracion/empresa/')
            else:
                messages.add_message(request, messages.WARNING, form.errors)

    else:
        try:
            datos = Empresa.objects.all()
            rif = datos[0].codigo_empresa
            form = EmpresaForm(instance=Empresa.objects.get(pk=rif))
            accion = 'update'
        except IndexError:
            form = EmpresaForm()


    context = {
        'form': form,
        'titulo': _('Configurar parámetro de la empresa'),
        'accion': accion,
        'mensaje' : _(mensaje)
    }
    return render(request, 'empresa.html', context)

class Departamento_Crear(CreateView):
    model = Departamento
    template_name = 'departamento.html'
    success_url = reverse_lazy('administracion:departamento_listado')
    form_class = DepartamentoForm

    def get_context_data(self, **kwargs):
        context = super(Departamento_Crear, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Registar departamentos')
        if 'accion' not in context:
            context['accion'] = _('Registrar')

        return context


class Departamento_Actualizar(UpdateView):
    model = Departamento
    template_name = 'departamento.html'
    success_url = reverse_lazy('administracion:departamento_listado')
    form_class = DepartamentoForm

    def get_context_data(self, **kwargs):
        context = super(Departamento_Actualizar,
                        self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Registar departamentos')
        if 'accion' not in context:
            context['accion'] = _('Actualizar')

        return context


class Departamento_Listado(ListView):
    model = Departamento
    template_name = 'departamento_listado.html'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(Departamento_Listado, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Listado de salones de clases')

        return context
class Concepto_Factura(CreateView):
    model = ConceptoFactura
    form_class = ConceptoFacturaForm
    template_name = 'concepto_factura.html'
    success_url = reverse_lazy('administracion:concepto_factura_listado')

    def get_context_data(self, **kwargs):
        context = super(Concepto_Factura, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Registar conceptos facturables')


        return context
class Concepto_Factura_Actualizar(UpdateView):
    model = ConceptoFactura
    form_class = ConceptoFacturaForm
    template_name = 'concepto_factura.html'
    success_url = reverse_lazy('administracion:concepto_factura_listado')

    def get_context_data(self, **kwargs):
        context = super(Concepto_Factura_Actualizar, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Actualizar conceptos facturables')
        if 'accion' not in context:
            context['accion'] = 'actualizar'

        return context

class Concepto_Factura_Listado(ListView):
    model = ConceptoFactura
    paginate_by = 10
    template_name = 'concepto_factura_listado.html'

    def get_context_data(self, **kwargs):
        context = super(Concepto_Factura_Listado, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = 'Listado de conceptos facturables'

        return context

class Concepto_Factura_Eliminar(DeleteView):
    model = ConceptoFactura
    success_url = reverse_lazy('administracion:concepto_factura_listado')
    template_name = 'concepto_factura_eliminar.html'

###### asignar costo a cursos ###########

def ajaxCostoCurso(request):
    xHTML = ''
    mensaje = ''
    if request.POST:
        pass
    if request.GET:
        if request.GET['accion'] == 'getCursos':
            try:
                if request.GET['frase'] is not None and len(request.GET['frase']) > 0:                   
                    Cursos = Curso.objects.filter(estado='A', descripcion__contains=request.GET['frase'])                 
                else:
                    Cursos = Curso.objects.filter(estado='A')
                paginator = Paginator(Cursos, 10)   
            except ObjectDoesNotExist:
                return HttpResponse(json.dumps({'mensaje': str(_('No hay cursos creados en el sistema'))}), content_type="application/json")
                
            try:
                page = request.GET['pagina']
                datos = paginator.page(page)                    
            except PageNotAnInteger:
                datos = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                datos = paginator.page(paginator.num_pages)
            paginas = datos.previous_page_number
            
            for cursos in datos:
                xHTML += '<tr>'
                xHTML += '<td><a style = "cursor:pointer;" onclick="seleccion_curso(\''+cursos.codigo_curso+'\',\''+cursos.descripcion+'\')">'+cursos.codigo_curso+'<a/></td>'
                xHTML += '<td>'+cursos.descripcion+'</td>'
                xHTML += '</tr>'
            return HttpResponse(json.dumps({'datos': xHTML,'mensaje': '','pagina':str(paginas)}), content_type="application/json")

        if request.GET['accion'] == 'getConceptos':
            ''' catalogo conceptos '''
            datos_conceptos = ConceptoFactura.objects.filter(estado='A')
            for concepto in datos_conceptos:
                xHTML += '<tr>'
                xHTML += '<td><a style = "cursor:pointer" onclick="seleccion_concepto(\''+concepto.codigo_concepto+'\',\''+concepto.descripcion+'\')">'+concepto.codigo_concepto+'</a></td>'
                xHTML += '<td>'+concepto.descripcion+'</td>'
                xHTML += '<td>'+str(concepto.monto_neto)+'</td>'
                xHTML += '</tr>'
            return HttpResponse(json.dumps({'datos': xHTML,'mensaje' : '' }), content_type="application/json")

        if request.GET['accion'] == 'getDatoConcepto':
            try:
                datos_concepto = ConceptoFactura.objects.get(codigo_concepto=request.GET['concepto'])
                return HttpResponse(json.dumps({'descripcion_concepto':datos_concepto.descripcion,'mensaje' : '' }), content_type="application/json")
            except ObjectDoesNotExist:
                mensaje = _('Código de concepto ingresado no existe')
                return HttpResponse(json.dumps({'mensaje': str(mensaje)}), content_type="application/json")

        if request.GET['accion'] == 'getDatosCurso':
            ''' Catalogo de curso '''
            #try:
            datos_curso = Curso.objects.get(codigo_curso=request.GET['curso'])
            datos = Nivel.objects.filter(codigo_curso=request.GET['curso'])

            for niveles in datos:
                xHTML += '<tr>'
                xHTML += '<td><input type = "hidden" name = "codigo_nivel[]" value= "'+niveles.codigo_nivel+'" >'+niveles.codigo_nivel+'</td>'
                xHTML += '<td>'+niveles.descripcion+'</td>'
                xHTML += '<td>'+str(niveles.meses_duracion)+' Meses</td>'
                xHTML += '<td><input type= "number" step="0.001" value = "'+str(niveles.monto_contado)+'" name = "monto_credito[]" id = "monto-contado-'+str(niveles.id)+'" onKeyPress="return soloNumeros(event, this.value)" class="monto_concepto_contado form-control"></td>'
                xHTML += '<td><input type= "number" step="0.001" value = "'+str(niveles.monto_credito)+'" name = "monto_contado[]" id = "monto-credito-'+str(niveles.id)+'" onKeyPress="return soloNumeros(event, this.value)" class="monto_concepto_credito form-control"></td>'
                xHTML += '</tr>'
            return HttpResponse(json.dumps({'datos': xHTML,'excento' : datos[0].excento_iva,
                                            'descripcion_curso':datos_curso.descripcion,
                                            'concepto_inscripcion':str(datos_curso.concepto_inscripcion),
                                            'mensaje' : '' }), content_type="application/json")
            #except ObjectDoesNotExist:
            #    mensaje = _('Código de curso ingresado no existe')
            #    return HttpResponse(json.dumps({'mensaje': str(mensaje)}), content_type="application/json")


def Validar_Costo_Curso(request):
    if not len(request.POST['codigo_nivel[]']) > 0:
        messages.add_message(request, messages.WARNING, _("Ingrese por favor un código de curso válido"))
        return False

    return True
def Guardar_Costo_Curso(request):
    if Validar_Costo_Curso(request):
        lista_nivel = request.POST.getlist('codigo_nivel[]')
        lista_credito = request.POST.getlist('monto_contado[]')
        lista_contado = request.POST.getlist('monto_credito[]')
        try:
            with transaction.atomic():
                Curso.objects.filter(codigo_curso=request.POST['codigo_curso']).update(
                    concepto_inscripcion=request.POST['concepto_inscripcion']
                );
                for i in range(0, len(lista_nivel)):
                    if 'excento_iva' in request.POST:
                        Nivel.objects.filter(codigo_nivel=lista_nivel[i]).update(
                            monto_credito=lista_credito[i],
                            monto_contado=lista_contado[i],
                            excento_iva=True,
                        )
                    else:
                        Nivel.objects.filter(codigo_nivel=lista_nivel[i]).update(
                            monto_credito=lista_credito[i],
                            monto_contado=lista_contado[i],
                            excento_iva=False
                        )

            return redirect('/administrativo/costo-curso/')
        except IntegrityError as e:
            messages.add_message(request, messages.WARNING, _("Ocurrió un error inesperado y no se logro registrar el costo del curso"))


def Costo_Curso(request):

    if request.is_ajax():
        return ajaxCostoCurso(request)

    if request.POST:
        Guardar_Costo_Curso(request)

    context = {
        'titulo' : _('Valor de curso'),
        'form' : CostoCursoForm
    }
    return render(request,'valor_curso.html', context)

###### fin signar costo a cursos ###########

class Cliente_Registro(CreateView):
    model = Cliente
    template_name = 'clientes.html'
    form_class = ClienteForm
    success_url = reverse_lazy('administracion:cliente_listado')

    def get_context_data(self, **kwargs):
        context = super(Cliente_Registro, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Registro de clientes')
        return context
    def form_invalid(self, form):
        if not form.is_valid():
            messages.add_message(self.request, messages.WARNING, form.errors)
        return self.render_to_response(self.get_context_data(form=form))

class Cliente_Actualizar(UpdateView):
    model = Cliente
    template_name = 'clientes.html'
    form_class = ClienteForm
    success_url = reverse_lazy('administracion:cliente_listado')

    def get_context_data(self, **kwargs):
        context = super(Cliente_Actualizar, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Registro de clientes')
        if 'accion' not in context:
            context['accion'] = 'actualizar'
        return context

    def form_invalid(self, form):
        if not form.is_valid():
            messages.add_message(self.request, messages.WARNING, form.errors)
        return self.render_to_response(self.get_context_data(form=form))

class Cliente_Listado(ListView):
    template_name = 'clientes_listado.html'
    paginate_by = 10
    model = Cliente
