from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from .views import *

urlpatterns = [
    url(r'^conceptos-factura/registrar/$', login_required(Concepto_Factura.as_view()), name='concepto_factura_crear'),
    url(r'^conceptos-factura/listado/$', login_required(Concepto_Factura_Listado.as_view()),
        name='concepto_factura_listado'),
    url(r'^conceptos-factura/editar/(?P<pk>\d+)/$', login_required(Concepto_Factura_Actualizar.as_view()),
        name='concepto_factura_editar'),
    url(r'^conceptos-factura/eliminar/(?P<pk>\d+)/$', login_required(Concepto_Factura_Eliminar.as_view()),
        name='concepto_factura_eliminar'),

    url(r'^costo-curso/$',login_required(Costo_Curso), name='costo_curso'),

    url(r'^cliente/registrar/$',login_required(Cliente_Registro.as_view()), name='cliente_crear'),
    url(r'^cliente/editar/(?P<pk>\d+)/$',login_required(Cliente_Actualizar.as_view()), name='cliente_actualizar'),
    url(r'^cliente/listado/$',login_required(Cliente_Listado.as_view()), name='cliente_listado'),
    
    url(r'^departamento/registrar/$',login_required(Departamento_Crear.as_view()), name='departamento_crear'),
    url(r'^departamento/editar/(?P<pk>\d+)/$', login_required(Departamento_Actualizar.as_view()),
        name='departamento_actualizar'),
    url(r'^departamento/listado', login_required(Departamento_Listado.as_view()), name='departamento_listado'),

    url(r'^configuracion/empresa/subir-logo/$', login_required(Subir_logo), name='logo_empresa'),
    url(r'^configuracion/empresa/$', login_required(configuracion_empresa), name='configurar_empresa'),
]
