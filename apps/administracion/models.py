from django.db import models
from django.utils.translation import ugettext_lazy as _


# Create your models here.
class Departamento(models.Model):
    ACTIVO = 'A'
    INACTIVO = 'I'
    STATUS = (
        (ACTIVO, 'Habilitado'),
        (INACTIVO, 'Inhabilitado'),
    )
    departamento = models.CharField(max_length=200, verbose_name=_('Departamento'), unique=True)
    estado = models.CharField(max_length=1, default='A',choices=STATUS, verbose_name=_('Estado'))

    def __str__(self):
        return self.departamento

    class Meta:
        db_table = 'tb_adm_departamento'
        verbose_name = 'Departamento'
        verbose_name_plural = 'Departamentos'


class Cliente(models.Model):
    ACTIVO = 'A'
    INACTIVO = 'I'
    STATUS = (
        (ACTIVO, 'Habilitado'),
        (INACTIVO, 'Inhabilitado'),
    )
    rif_cliente = models.CharField(max_length=14, null=False, blank=False, db_index=True, unique=True,
                                   verbose_name=_('Cédula / Rif'))
    razon_social = models.CharField(max_length=60, null=False, blank=False, verbose_name=_('Nombre o Razon Social'))
    direccion_habitacion = models.CharField(max_length=200, null=False, blank=False, verbose_name=_('Dirección'))
    telefono = models.CharField(max_length=12, verbose_name=_('Número de teléfono fijo'))
    telefono_opcional = models.CharField(null=True, blank=True, max_length=12,
                                         verbose_name=_('Número de teléfono opcional'))
    movil = models.CharField(max_length=12, verbose_name=_('Número de teléfono movil'))
    correo = models.CharField(max_length=60, null=False, blank=False, verbose_name=_('Correo electrónico'))
    fecha_nacimiento = models.DateField(null=True, blank=True, verbose_name=_('Fecha de nacimiento'))
    empresa_labora = models.CharField(max_length=60, null=False, blank=False, verbose_name=_('Empresa donde labora'),
                                      default=_('NO APLICA'))
    cargo = models.CharField(max_length=60, null=False, blank=False, verbose_name=_('Cargo que ocupa'),
                             default=_('NO APLICA'))
    antiguedad = models.CharField(max_length=60, null=False, blank=False, verbose_name=_('Antiguedad en la empresa'),
                                  default=_('NO APLICA'))
    estado = models.CharField(max_length=1, choices=STATUS, default='A')

    class Meta:
        db_table = 'tb_adm_cliente'
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'


class ConceptoFactura(models.Model):
    ACTIVO = 'A'
    INACTIVO = 'I'
    STATUS = (
        (ACTIVO, _('Activo')),
        (INACTIVO, _('Inhabilitado')),
    )

    codigo_concepto = models.CharField(max_length=4, db_index=True, blank=False, null=False, unique=True,
                                       verbose_name=_('Código de concepto'))
    descripcion = models.CharField(max_length=30, blank=-False, null=False, verbose_name=_('Descripción del concepto'))
    excento_iva = models.BooleanField(default=False, verbose_name=_('Excento de I.V.A'))
    monto_neto = models.DecimalField(max_digits=10, decimal_places=3, null=False, blank=False,
                                     verbose_name='Monto sin I.V.A')

    estado = models.CharField(max_length=1, default='A', choices=STATUS, verbose_name=_('Estado'))

    def __str__(self):
        return self.codigo_concepto + ' / ' + self.descripcion

    class Meta:
        db_table = 'tb_adm_concepto_factura'
        verbose_name = 'Concepto'
        verbose_name_plural = 'Conceptos'

class Empresa(models.Model):
    codigo_empresa = models.CharField(max_length=20, primary_key=True, verbose_name='Rif')
    logo = models.CharField( blank=True, null=True, max_length=200)
    razon_social = models.CharField(max_length=60, null=False, blank=False, verbose_name='Razon social')
    direccion_fiscal = models.CharField(max_length=200, null=False, blank=False, verbose_name='Dirección fiscal')
    control_factura = models.BigIntegerField(verbose_name='Número de control de facturación')
    correlativo_factura = models.BigIntegerField(verbose_name='Número de factura')
    correlativo_contrato = models.BigIntegerField(verbose_name='Número de control de contratos')
    tasa_iva = models.DecimalField(max_digits=6, decimal_places=2, null=False, blank=False, verbose_name='Tasa de iva')

    class Meta:
        db_table = 'tb_adm_empresa'
        verbose_name = 'Empresa'