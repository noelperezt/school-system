from django import forms
from .models import Empresa, ConceptoFactura, Cliente, Departamento
from django.utils.translation import ugettext_lazy as _

class EmpresaForm(forms.ModelForm):
    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)
        for campo in self.fields:
            self.fields[campo].widget.attrs.update({'required' : 'required'})
            self.fields[campo].widget.attrs.update({'class' : 'form-control'})

    class Meta:
        model = Empresa
        fields = ['codigo_empresa', 'razon_social', 'direccion_fiscal', 'control_factura','logo',
                  'correlativo_factura', 'correlativo_contrato', 'tasa_iva']

class DepartamentoForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for campo in self.fields:
            self.fields[campo].widget.attrs.update(
                {
                    'class' : 'form-control',
                    'required': 'required',
                 }
            )
    class Meta:
        model = Departamento
        fields = ['departamento', 'estado']


class ConceptoFacturaForm(forms.ModelForm):
    estado = forms.ChoiceField(choices=ConceptoFactura.STATUS)
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for campo in self.fields:
            if campo != 'excento_iva':
                self.fields[campo].widget.attrs.update(
                    {
                        'class' : 'form-control',
                        'required': 'required',
                     }
                )

        self.fields['excento_iva'].widget.attrs.update({'class' : 'check'})

    class Meta:
        model = ConceptoFactura
        fields = ['codigo_concepto', 'descripcion', 'excento_iva', 'estado','monto_neto',]


class CostoCursoForm(forms.Form):
    codigo_curso = forms.CharField(max_length=4, label=_('Código de curso'))
    descripcion = forms.CharField(label=_('Descripción'))
    concepto_inscripcion = forms.CharField(max_length=4, label=_('Concepto Inscripción'))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for campo in self.fields:
            if campo != 'excento_iva':
                self.fields[campo].widget.attrs.update(
                    {
                        'class' : 'form-control',
                        'required': 'required',
                     }
                )


class ClienteForm(forms.ModelForm):
    INPUT_FORMAT = (
        ('%d/%m/%Y','%m/%d/%Y')
    )
    fecha_nacimiento = forms.DateField(input_formats = INPUT_FORMAT)
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for campo in self.fields:
            if campo != 'fecha_nacimiento':
                self.fields[campo].widget.attrs.update(
                    {
                        'class' : 'form-control',
                        'required': 'required',
                     }
                )
            else:
                self.fields[campo].widget.attrs.update(
                    {
                        'class' : 'form-control',
                        'id' : 'single_cal2'
                     }
                )
    class Meta:
        
        model = Cliente
        fields = '__all__'
        
